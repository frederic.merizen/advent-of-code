use commons::{parse_input, Result};
use std::collections::VecDeque;

struct ShiftyVec<T> {
    offset: usize,
    storage: VecDeque<T>,
}

impl<T> ShiftyVec<T> {
    fn new() -> Self {
        ShiftyVec {
            offset: 0,
            storage: VecDeque::new(),
        }
    }

    fn len(&self) -> usize {
        self.storage.len()
    }

    #[cfg(test)]
    fn to_vec(&self) -> Vec<&T> {
        if self.offset == 0 {
            self.storage.iter().collect()
        } else {
            let head = self.storage.iter().skip(self.storage.len() - self.offset);
            let tail = self.storage.iter().take(self.storage.len() - self.offset);
            head.chain(tail).collect()
        }
    }

    fn forward(&mut self, pos: usize) {
        for _ in self.offset..pos {
            let x = self.storage.pop_front().unwrap();
            self.storage.push_back(x);
        }
        self.offset = pos;
    }

    fn rewind(&mut self, pos: usize) {
        for _ in pos..self.offset {
            let x = self.storage.pop_back().unwrap();
            self.storage.push_front(x);
        }
        self.offset = pos;
    }

    fn insert(&mut self, pos: usize, val: T) {
        if pos == self.storage.len() {
            self.rewind(0);
            self.storage.push_back(val);
        } else {
            if pos > self.offset {
                self.forward(pos)
            } else if pos < self.offset {
                self.rewind(pos)
            }
            self.storage.push_front(val);
        }
    }

    fn remove(&mut self, pos: usize) -> T {
        if pos > self.offset {
            self.forward(pos);
        } else if pos < self.offset {
            self.rewind(pos);
        }
        self.storage.pop_front().unwrap()
    }
}

struct Track {
    ring: ShiftyVec<usize>,
    current_pos: usize,
    current_val: usize,
}

impl Track {
    fn new() -> Self {
        let mut ring = ShiftyVec::new();
        ring.insert(0, 0);
        Track {
            ring,
            current_pos: 0,
            current_val: 0,
        }
    }

    fn play(&mut self) -> usize {
        self.current_val += 1;
        if self.current_val % 23 == 0 {
            self.current_pos = (self.ring.len() + self.current_pos - 7) % self.ring.len();
            self.ring.remove(self.current_pos) + self.current_val
        } else {
            self.current_pos = (self.current_pos + 2) % self.ring.len();
            self.ring.insert(self.current_pos, self.current_val);
            0
        }
    }
}

struct Game {
    next_player: usize,
    player_count: usize,
    scores: Vec<usize>,
    track: Track,
}

impl Game {
    fn new(player_count: usize) -> Game {
        Game {
            next_player: 0,
            player_count,
            scores: vec![0; player_count],
            track: Track::new(),
        }
    }

    fn play(&mut self) {
        self.scores[self.next_player] += self.track.play();
        self.next_player = (self.next_player + 1) % self.player_count;
    }
}

fn hi_score(player_count: usize, highest_marble: usize) -> usize {
    assert!(player_count > 0);
    assert!(highest_marble > 0);
    let mut g = Game::new(player_count);
    for _ in 0..highest_marble {
        g.play();
    }
    *g.scores.iter().max().unwrap()
}

mod parser {
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{do_parse, eol, tag, terminated, IResult};

    #[rustfmt::skip]
    pub(super) fn line(i: CompleteStr) -> IResult<CompleteStr, (usize, usize)> {
        do_parse!(i,
            players: uint                                    >>
                     tag!(" players; last marble is worth ") >>
            points:  uint                                    >>
                     tag!(" points")                         >>
            (players, points)
        )
    }

    pub(super) fn input(i: CompleteStr) -> IResult<CompleteStr, (usize, usize)> {
        terminated!(i, line, eol)
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_read_line() {
            assert_eq!(
                (411, 71058),
                line(CompleteStr("411 players; last marble is worth 71058 points"))
                    .unwrap()
                    .1
            );
        }
    }
}

fn read_input<'a>() -> Result<'a, (usize, usize)> {
    parse_input!(parser::input)
}

fn main() {
    let (players, points) = read_input().unwrap();
    println!("High score is {}", hi_score(players, points));
    println!("New high score is {}", hi_score(players, 100 * points));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_track() {
        assert_eq!(vec![&0], Track::new().ring.to_vec());
    }

    #[test]
    fn test_play_marble() {
        let mut t = Track::new();
        let s = t.play();
        assert_eq!(vec![&1, &0], t.ring.to_vec());
        assert_eq!(s, 0);
        let s = t.play();
        assert_eq!(vec![&2, &1, &0], t.ring.to_vec());
        assert_eq!(s, 0);
        let s = t.play();
        assert_eq!(vec![&2, &1, &3, &0], t.ring.to_vec());
        assert_eq!(s, 0);
        let s = t.play();
        assert_eq!(vec![&4, &2, &1, &3, &0], t.ring.to_vec());
        assert_eq!(s, 0);
    }

    #[test]
    fn test_play_23rd_marble() {
        let mut t = Track::new();
        for _ in 0..22 {
            t.play();
        }
        let s = t.play();
        assert_eq!(
            vec![
                &16, &8, &17, &4, &18, &19, &2, &20, &10, &21, &5, //
                &22, &11, &1, &12, &6, &13, &3, &14, &7, &15, &0
            ],
            t.ring.to_vec()
        );
        assert_eq!(s, 32);
        let s = t.play();
        assert_eq!(
            vec![
                &16, &8, &17, &4, &18, &19, &2, &24, &20, &10, &21, &5, //
                &22, &11, &1, &12, &6, &13, &3, &14, &7, &15, &0
            ],
            t.ring.to_vec()
        );
        assert_eq!(s, 0);
    }

    #[test]
    fn test_keep_player_scores() {
        let mut g = Game::new(9);
        for _ in 0..25 {
            g.play();
        }
        assert_eq!(7, g.next_player);
        assert_eq!(32, g.scores[4]);
    }

    #[test]
    fn test_hi_score() {
        assert_eq!(32, hi_score(9, 25));
        assert_eq!(8317, hi_score(10, 1618));
        assert_eq!(146_373, hi_score(13, 7999));
        assert_eq!(2764, hi_score(17, 1104));
        assert_eq!(54_718, hi_score(21, 6111));
        assert_eq!(37_305, hi_score(30, 5807));
    }

    #[test]
    fn test_shifty_vec() {
        let mut v = ShiftyVec::new();
        assert_eq!(Vec::<&usize>::new(), v.to_vec());
        v.insert(0, 4);
        assert_eq!(vec![&4], v.to_vec());
        v.insert(0, 2);
        assert_eq!(vec![&2, &4], v.to_vec());
        v.insert(2, 3);
        assert_eq!(vec![&2, &4, &3], v.to_vec());
        let x = v.remove(1);
        assert_eq!(vec![&2, &3], v.to_vec());
        assert_eq!(4, x);
        v.insert(0, 1);
        assert_eq!(vec![&1, &2, &3], v.to_vec());
        v.insert(1, 5);
        assert_eq!(vec![&1, &5, &2, &3], v.to_vec());
        let x = v.remove(0);
        assert_eq!(vec![&5, &2, &3], v.to_vec());
        assert_eq!(1, x);
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref PARAMETERS: (usize, usize) = read_input().unwrap();
    }

    #[test]
    fn test_small_game() {
        assert_eq!(424_639, hi_score(PARAMETERS.0, PARAMETERS.1));
    }

    #[test]
    fn test_big_game() {
        assert_eq!(3_516_007_333, hi_score(PARAMETERS.0, 100 * PARAMETERS.1));
    }
}
