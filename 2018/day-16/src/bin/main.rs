use commons::parse_input;
use elf::uninstantiated::Instruction as Operation;
use elf::INSTRUCTIONS;
use maplit::hashmap;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::io;

fn main() {
    let (samples, code) = parse_input!(parser::problem_input).unwrap();
    println!(
        "Samples matching three or more opcodes: {}",
        count_samples_matching_3_opcodes_or_more(&samples)
    );
    let opcodes = find_opcode_numbers(&samples).unwrap();
    println!(
        "R0 after running problem code {}",
        r0_after_code(&opcodes, &code).unwrap()
    );
    dump_as_rust(&opcodes, &code).unwrap();
}

fn dump_as_rust<'a, C: IntoIterator<Item = &'a Instruction>>(
    opcodes: &HashMap<u8, Operation>,
    code: C,
) -> io::Result<()> {
    use std::fs::File;
    use std::io::Write;

    let mut file = File::create("src/bin/elf.rs")?;
    let elfcode = elf::instantiated::ElfCode::without_ip(
        code.into_iter()
            .map(|instruction| instruction.decode(opcodes))
            .collect(),
    );
    write!(file, "{}", elfcode)
}

fn r0_after_code<'a, C: IntoIterator<Item = &'a Instruction>>(
    opcodes: &HashMap<u8, Operation>,
    code: C,
) -> Result<usize, Unhandled> {
    let mut regs = [0; 4];
    run(opcodes, &mut regs, code);
    Ok(regs[0])
}

fn run<'a, T: IntoIterator<Item = &'a Instruction>>(
    opcodes: &HashMap<u8, Operation>,
    regs: &mut Registers,
    code: T,
) {
    for instruction in code {
        instruction.run(regs, opcodes);
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Instruction {
    opcode: u8,
    input_a: usize,
    input_b: usize,
    output: usize,
}

impl Instruction {
    fn decode(&self, opcodes: &HashMap<u8, Operation>) -> elf::instantiated::Instruction {
        opcodes[&self.opcode].instantiate(self.input_a, self.input_b, self.output)
    }

    fn run(&self, regs: &mut Registers, opcodes: &HashMap<u8, Operation>) {
        self.decode(opcodes).run(regs)
    }
}

type Registers = [usize; 4];

#[derive(Clone, Debug, Eq, PartialEq)]
struct Sample {
    before: Registers,
    instruction: Instruction,
    after: Registers,
}

impl Sample {
    fn matches_opcode(&self, op: Operation) -> bool {
        let mut r = self.before;
        op.run(
            &mut r,
            self.instruction.input_a,
            self.instruction.input_b,
            self.instruction.output,
        );
        r == self.after
    }

    fn count_matching_opcodes(&self) -> usize {
        self.keep_matching_opcodes(INSTRUCTIONS.iter().cloned())
            .iter()
            .count()
    }

    fn keep_matching_opcodes<T>(&self, opcodes: T) -> HashSet<Operation>
    where
        T: IntoIterator<Item = Operation>,
    {
        opcodes
            .into_iter()
            .filter_map(|o| if self.matches_opcode(o) { Some(o) } else { None })
            .collect()
    }
}

fn count_samples_matching_3_opcodes_or_more<'a, T: IntoIterator<Item = &'a Sample>>(
    samples: T,
) -> usize {
    samples
        .into_iter()
        .filter(|s| s.count_matching_opcodes() >= 3)
        .count()
}

#[derive(Debug)]
struct Unhandled;

impl Display for Unhandled {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        "The input was too complex for this simplistic solver".fmt(fmt)
    }
}

impl Error for Unhandled {}

fn find_opcode_numbers<'a, T: IntoIterator<Item = &'a Sample>>(
    samples: T,
) -> Result<HashMap<u8, Operation>, Unhandled> {
    let mut compatible_opcodes = find_compatible_opcodes(samples);
    let mut single_opcodes = compatible_opcodes
        .iter()
        .filter_map(|(&n, ops)| {
            if ops.len() == 1 {
                Some((n, ops.iter().cloned().next().unwrap()))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();
    while let Some((single_n, single_op)) = single_opcodes.pop() {
        for (&n, ops) in compatible_opcodes.iter_mut().filter(|(&n, _)| n != single_n) {
            if ops.remove(&single_op) && ops.len() == 1 {
                single_opcodes.push((n, ops.iter().cloned().next().unwrap()));
            }
        }
    }
    compatible_opcodes
        .into_iter()
        .map(|(n, ops)| {
            if ops.len() == 1 {
                Ok((n, ops.iter().cloned().next().unwrap()))
            } else {
                Err(Unhandled)
            }
        })
        .collect()
}

fn find_compatible_opcodes<'a, T: IntoIterator<Item = &'a Sample>>(
    samples: T,
) -> HashMap<u8, HashSet<Operation>> {
    let mut compatible_opcodes_by_number = hashmap! {};
    for sample in samples {
        let opcodes = compatible_opcodes_by_number
            .entry(sample.instruction.opcode)
            .or_insert(INSTRUCTIONS.iter().cloned().collect::<HashSet<_>>());
        *opcodes = sample.keep_matching_opcodes(opcodes.iter().cloned());
    }
    compatible_opcodes_by_number
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_matching_opcodes() {
        let sample = Sample {
            before: [3, 2, 1, 1],
            instruction: Instruction {
                opcode: 9,
                input_a: 2,
                input_b: 1,
                output: 2,
            },
            after: [3, 2, 2, 1],
        };

        assert_eq!(3, sample.count_matching_opcodes());
    }

    #[test]
    fn test_count_samples_matching_3_opcodes_or_more() {
        let samples = vec![Sample {
            before: [3, 2, 1, 1],
            instruction: Instruction {
                opcode: 9,
                input_a: 2,
                input_b: 1,
                output: 2,
            },
            after: [3, 2, 2, 1],
        }];

        assert_eq!(1, count_samples_matching_3_opcodes_or_more(&samples));
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref INPUT: (Vec<Sample>, Vec<Instruction>) = parse_input!(parser::problem_input).unwrap();
    }

    fn samples() -> &'static Vec<Sample> {
        &INPUT.0
    }

    fn code() -> &'static Vec<Instruction> {
        &INPUT.1
    }

    #[test]
    fn test_count_samples_matching_3_opcodes_or_more() {
        assert_eq!(560, count_samples_matching_3_opcodes_or_more(samples()));
    }

    #[test]
    fn test_find_opcode_numbers() {
        use elf::*;

        let opcodes_by_num = find_opcode_numbers(samples()).unwrap();
        assert!(opcodes_by_num[&0x0] == BANI);
        assert!(opcodes_by_num[&0x1] == GTRI);
        assert!(opcodes_by_num[&0x2] == SETI);
        assert!(opcodes_by_num[&0x3] == EQIR);
        assert!(opcodes_by_num[&0x4] == EQRR);
        assert!(opcodes_by_num[&0x5] == BORR);
        assert!(opcodes_by_num[&0x6] == BORI);
        assert!(opcodes_by_num[&0x7] == BANR);
        assert!(opcodes_by_num[&0x8] == MULI);
        assert!(opcodes_by_num[&0x9] == EQRI);
        assert!(opcodes_by_num[&0xa] == MULR);
        assert!(opcodes_by_num[&0xb] == GTRR);
        assert!(opcodes_by_num[&0xc] == SETR);
        assert!(opcodes_by_num[&0xd] == ADDR);
        assert!(opcodes_by_num[&0xe] == GTIR);
        assert!(opcodes_by_num[&0xf] == ADDI);
    }

    #[test]
    fn test_r0_after_code() {
        let opcodes = find_opcode_numbers(samples()).unwrap();
        assert_eq!(622, r0_after_code(&opcodes, code()).unwrap());
    }
}

mod parser {
    use super::{Instruction, Registers, Sample};
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{
        char, count, delimited, do_parse, eol, is_a, many0, named, opt, tag, terminated, IResult,
    };

    named!(space<CompleteStr, Option<CompleteStr>>, opt!(is_a!(" ")));
    named!(comma<CompleteStr, char>, delimited!(space, char!(','), space));

    named!(register_values<CompleteStr, Registers>,
           do_parse!(
                   char!('[') >>
               r0: uint       >>
                   comma      >>
               r1: uint       >>
                   comma      >>
               r2: uint       >>
                   comma      >>
               r3: uint       >>
                   char!(']') >>
               ([r0, r1, r2, r3])
           )
    );

    named!(instruction<CompleteStr, Instruction>,
           do_parse!(
               opcode:  uint  >>
                        space >>
               input_a: uint  >>
                        space >>
               input_b: uint  >>
                        space >>
               output:  uint  >>
               (Instruction{ opcode, input_a, input_b, output })
           )
    );

    named!(sample<CompleteStr, Sample>,
           do_parse!(
                            tag!("Before:") >>
                            space           >>
               before:      register_values >>
                            eol             >>
               instruction: instruction     >>
                            eol             >>
                            tag!("After:")  >>
                            space           >>
               after:       register_values >>
                            eol             >>
               (Sample{ before, instruction, after })
           )
    );

    #[rustfmt::skip]
    pub(super) fn problem_input(i: CompleteStr) -> IResult<CompleteStr, (Vec<Sample>, Vec<Instruction>)> {
           do_parse!(i,
               samples:      many0!(terminated!(sample, eol    ))      >>
                             count!(eol    , 2)                        >>
               instructions: many0!(terminated!(instruction, eol    )) >>
               (samples, instructions)
           )
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        #[test]
        fn test_register_values() {
            assert_eq!(
                Ok((CompleteStr(""), [1, 2, 3, 4])),
                register_values(CompleteStr("[1, 2, 3, 4]"))
            );
        }

        #[test]
        fn test_instruction() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    Instruction {
                        opcode: 8,
                        input_a: 7,
                        input_b: 6,
                        output: 5
                    }
                )),
                instruction(CompleteStr("8 7 6 5"))
            );
        }

        #[test]
        fn test_sample() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    Sample {
                        before: [11, 12, 13, 14],
                        instruction: Instruction {
                            opcode: 2,
                            input_a: 7,
                            input_b: 1,
                            output: 3,
                        },
                        after: [21, 22, 23, 24],
                    }
                )),
                sample(CompleteStr(indoc!(
                    "
                    Before: [11, 12, 13, 14]
                    2 7 1 3
                    After: [21, 22, 23, 24]
                    "
                )))
            );
        }

        #[test]
        fn test_problem_input() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    (
                        vec![
                            Sample {
                                before: [11, 12, 13, 14],
                                instruction: Instruction {
                                    opcode: 2,
                                    input_a: 7,
                                    input_b: 1,
                                    output: 3,
                                },
                                after: [21, 22, 23, 24],
                            },
                            Sample {
                                before: [31, 32, 33, 34],
                                instruction: Instruction {
                                    opcode: 3,
                                    input_a: 0,
                                    input_b: 8,
                                    output: 4,
                                },
                                after: [41, 42, 43, 44],
                            }
                        ],
                        vec![
                            Instruction {
                                opcode: 9,
                                input_a: 8,
                                input_b: 7,
                                output: 6,
                            },
                            Instruction {
                                opcode: 15,
                                input_a: 4,
                                input_b: 3,
                                output: 2,
                            }
                        ]
                    )
                )),
                problem_input(CompleteStr(indoc!(
                    "
                    Before: [11, 12, 13, 14]
                    2 7 1 3
                    After: [21, 22, 23, 24]

                    Before: [31, 32, 33, 34]
                    3 0 8 4
                    After: [41, 42, 43, 44]



                    9 8 7 6
                    15 4 3 2
                    "
                )))
            );
        }
    }
}
