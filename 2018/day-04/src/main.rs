use chrono::{Duration, NaiveDateTime, NaiveTime, Timelike};
use commons::{lines, parse_input, Result};
use std::collections::HashMap;
use std::fmt;
use Event::*;

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
struct GuardId(u16);

#[derive(Debug)]
enum Event {
    FallsAsleep,
    WakesUp,
    BeginsShift(GuardId),
}

impl fmt::Display for Event {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            FallsAsleep => write!(f, "falls asleep"),
            WakesUp => write!(f, "wakes up"),
            BeginsShift(GuardId(id)) => write!(f, "Guard #{} begins shift", id),
        }
    }
}

struct EventInTime {
    date_time: NaiveDateTime,
    event: Event,
}

impl fmt::Display for EventInTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}] {}", self.date_time.format("%Y-%m-%d %H:%M"), self.event)
    }
}

mod parser {
    use super::{Event, EventInTime, GuardId};
    use chrono::{NaiveDate, NaiveDateTime};
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{alt, char, delimited, do_parse, map, map_opt, named, tag, value, IResult};
    use Event::*;

    fn make_naive_datetime(
        (year, month, day, hours, minutes): (i32, u32, u32, u32, u32),
    ) -> Option<NaiveDateTime> {
        NaiveDate::from_ymd_opt(year, month, day)?.and_hms_opt(hours, minutes, 0)
    }

    named!(timestamp<CompleteStr, NaiveDateTime>,
           map_opt!(
               do_parse!(
                   char!('[') >>
                       year:    uint       >>
                       char!('-') >>
                       month:   uint       >>
                       char!('-') >>
                       day:     uint       >>
                       char!(' ') >>
                       hours:   uint       >>
                       char!(':') >>
                       minutes: uint       >>
                       char!(']') >>
                       (year, month, day, hours, minutes)
               ),
               make_naive_datetime
           )
    );

    named!(falls_asleep<CompleteStr, Event>,
           value!(FallsAsleep, tag!("falls asleep"))
    );

    named!(wakes_up<CompleteStr, Event>,
           value!(WakesUp, tag!("wakes up"))
    );

    named!(begins_shift<CompleteStr, Event>,
           delimited!(
               tag!("Guard #"),
               map!(uint, |i| BeginsShift(GuardId(i))),
               tag!(" begins shift")
           )
    );

    #[rustfmt::skip]
    pub(super) fn timed_event(i: CompleteStr) -> IResult<CompleteStr, EventInTime> {
        do_parse!(i,
            date_time: timestamp                                    >>
                       char!(' ')                                   >>
            event:     alt!(falls_asleep | wakes_up | begins_shift) >>
            (EventInTime{ date_time, event })
        )
    }
}

fn read_input<'a>() -> Result<'a, Vec<EventInTime>> {
    let mut events = parse_input!(lines!(parser::timed_event))?;
    events.sort_unstable_by_key(|e| e.date_time);
    Ok(events)
}

struct State {
    guard_id: GuardId,
    sleeping_since: Option<NaiveTime>,
}

struct SleepPeriod {
    from: NaiveTime,
    to: NaiveTime,
}

type GuardSleepPeriods = HashMap<GuardId, Vec<SleepPeriod>>;
fn add_sleep(sleep: &mut GuardSleepPeriods, guard_id: GuardId, from: NaiveTime, to: NaiveTime) {
    sleep
        .entry(guard_id)
        .or_insert_with(Vec::new)
        .push(SleepPeriod { from, to });
}

fn sleep_periods(events: &[EventInTime]) -> GuardSleepPeriods {
    let mut result = GuardSleepPeriods::new();
    let final_state = events.iter().fold(
        State {
            guard_id: GuardId(0),
            sleeping_since: None,
        },
        |s, e| match e.event {
            BeginsShift(id) => {
                if let Some(t) = s.sleeping_since {
                    add_sleep(&mut result, s.guard_id, t, NaiveTime::from_hms(1, 0, 0));
                }
                State {
                    guard_id: id,
                    sleeping_since: None,
                }
            }
            WakesUp => {
                if let Some(t) = s.sleeping_since {
                    add_sleep(&mut result, s.guard_id, t, e.date_time.time());
                }
                State {
                    sleeping_since: None,
                    ..s
                }
            }
            FallsAsleep => State {
                sleeping_since: s.sleeping_since.or_else(|| Some(e.date_time.time())),
                ..s
            },
        },
    );
    if let Some(t) = final_state.sleeping_since {
        add_sleep(&mut result, final_state.guard_id, t, NaiveTime::from_hms(1, 0, 0));
    }
    result
}

fn most_sleepy(sleep: &GuardSleepPeriods) -> Option<GuardId> {
    let mut times = HashMap::new();

    for (&id, periods) in sleep {
        let time = periods
            .iter()
            .map(|SleepPeriod { from, to }| *to - *from)
            .fold(Duration::zero(), |s, d| s + d);
        times.insert(id, time);
    }

    let (&id, _) = times.iter().max_by_key(|&(_, t)| t)?;
    Some(id)
}

fn sleepiest_minute(sleep: &[SleepPeriod]) -> Option<u8> {
    let mut minutes = HashMap::new();

    for &SleepPeriod { from, to } in sleep {
        for m in from.minute()..to.minute() {
            *minutes.entry(m as u8).or_insert(0) += 1;
        }
    }

    let (&m, _) = minutes.iter().max_by_key(|&(_, c)| c)?;
    Some(m)
}

fn strategy_1(sleep: &GuardSleepPeriods) -> Option<u16> {
    let sleepy_guard_id = most_sleepy(sleep)?;
    let sleepy_minute = sleepiest_minute(&sleep[&sleepy_guard_id])?;
    let GuardId(id) = sleepy_guard_id;
    Some(id * u16::from(sleepy_minute))
}

fn sleepiest_guard_minute(sleep: &GuardSleepPeriods) -> Option<(GuardId, u8)> {
    let mut guard_minutes = HashMap::new();

    for (&id, periods) in sleep {
        for &SleepPeriod { from, to } in periods {
            for m in from.minute()..to.minute() {
                *guard_minutes.entry((id, m as u8)).or_insert(0) += 1;
            }
        }
    }

    let (&k, _) = guard_minutes.iter().max_by_key(|&(_, c)| c)?;
    Some(k)
}

fn strategy_2(sleep: &GuardSleepPeriods) -> Option<u16> {
    let (GuardId(id), sleepy_minute) = sleepiest_guard_minute(sleep)?;
    Some(id * u16::from(sleepy_minute))
}

fn main() {
    let events = read_input().unwrap();
    let sleep = sleep_periods(&events);
    println!("Strategy 1 : {}", strategy_1(&sleep).unwrap());
    println!("Strategy 2 : {}", strategy_2(&sleep).unwrap());
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref SLEEP_PERIODS: GuardSleepPeriods = sleep_periods(&read_input().unwrap());
    }

    #[test]
    fn test_strategy1() {
        assert_eq!(21083, strategy_1(&*SLEEP_PERIODS).unwrap());
    }

    #[test]
    fn test_strategy2() {
        assert_eq!(53024, strategy_2(&*SLEEP_PERIODS).unwrap());
    }
}
