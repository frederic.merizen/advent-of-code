use nom;
use nom::types::CompleteStr;
use std::error::{self, Error as StdError};
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use Error::*;

pub type Result<'a, T> = std::result::Result<T, Error<'a>>;

#[derive(Debug)]
pub enum Error<'a> {
    InvalidInput,
    Io(io::Error),
    Parsing(nom::Err<CompleteStr<'a>>),
}

impl<'a> fmt::Display for Error<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl<'a> error::Error for Error<'a> {
    fn description(&self) -> &str {
        match self {
            InvalidInput => "Invalid input",
            Parsing(_) => "Syntax error",
            Io(_) => "Io failure",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match self {
            Io(cause) => Some(cause),
            Parsing(cause) => Some(cause),
            _ => None,
        }
    }
}

impl<'a> std::convert::From<io::Error> for Error<'a> {
    fn from(e: io::Error) -> Self {
        Io(e)
    }
}

impl<'a, P> std::convert::From<nom::Err<P>> for Error<'a> {
    fn from(_: nom::Err<P>) -> Self {
        InvalidInput
    }
}

pub trait OrInvalid<'a, T> {
    fn or_invalid(self) -> Result<'a, T>;
}

impl<'a, T> OrInvalid<'a, T> for Option<T> {
    fn or_invalid(self) -> Result<'a, T> {
        self.ok_or(InvalidInput)
    }
}

pub struct InputIterator(io::Lines<Box<BufReader<Box<File>>>>);

impl Iterator for InputIterator {
    type Item = Result<'static, String>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|r| Ok(r?))
    }
}

pub fn input_iterator() -> Result<'static, InputIterator> {
    let raw = Box::new(File::open("input.txt")?);
    let buf = Box::new(BufReader::new(raw));
    let lines = buf.lines();
    Ok(InputIterator(lines))
}

#[macro_export]
macro_rules! nom_call(
    ($i:expr, $f: expr) => (
        nom::call!($i, $f)
    )
);

#[macro_export]
macro_rules! nom_many1(
    ($i:expr, $submac:ident!( $($args:tt)* )) => (
        nom::many1!($i, $submac!($($args)*))
    )
);

#[macro_export]
macro_rules! nom_terminated(
    ($i:expr, $submac:ident!( $($args:tt)* ), $submac2:ident!( $($args2:tt)* )) => (
        nom::terminated!($i, $submac!($($args)*), $submac2!($($args2)*))
    )
);

#[macro_export(local_inner_macros)]
macro_rules! lines(
    ($i:expr, $submac:ident!( $($args:tt)* )) => ({
        nom_many1!($i, nom_terminated!($submac!($($args)*), nom_call!(nom::eol)))
    });
    ($i:expr, $f:expr) => (
        lines!($i, nom_call!($f));
    );
);

#[macro_export(local_inner_macros)]
macro_rules! parse_input(
    ($submac:ident!( $($args:tt)* )) => ({
        #[allow(clippy::redundant_closure)]
        $crate::parser::parse_input(|i| $submac!(i, $($args)*))
    });
    ($f:expr) => (
        parse_input!(nom_call!($f));
    );
);

pub mod parser {
    use super::{InvalidInput, Result};
    use nom::types::CompleteStr;
    use nom::{alt, char, digit, map_res, opt, pair, recognize, IResult};
    use std::fs;
    use std::str::FromStr;

    pub fn uint<U>(i: CompleteStr) -> IResult<CompleteStr, U>
    where
        U: FromStr,
    {
        map_res!(i, digit, |c: CompleteStr| c.parse())
    }

    pub fn int<I>(i: CompleteStr) -> IResult<CompleteStr, I>
    where
        I: FromStr,
    {
        map_res!(
            i,
            recognize!(pair!(opt!(alt!(char!('-') | char!('+'))), digit)),
            |c: CompleteStr| c.parse()
        )
    }

    pub fn assert_matched_all<'a, S, T>(r: IResult<S, T>) -> Result<'a, T>
    where
        S: AsRef<str>,
    {
        if let Ok((remaining, t)) = r {
            if remaining.as_ref().is_empty() {
                return Ok(t);
            }
        }
        Err(InvalidInput)
    }

    pub fn parse_input<'a, P, T>(p: P) -> Result<'a, T>
    where
        P: Fn(CompleteStr) -> nom::IResult<CompleteStr, T>,
    {
        let input = fs::read_to_string("input.txt")?;
        assert_matched_all(p(CompleteStr(&input)))
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use nom::char;

        #[test]
        fn test_reject_incomplete_match() {
            let res = char!("xa", 'x');
            assert!(res.is_ok());
            assert!(assert_matched_all(res).is_err());
        }
    }
}
