use commons::parse_input;
use itertools::Itertools;
use std::cmp::{Eq, Ord, Ordering};
use std::collections::BinaryHeap;
use std::ops::Sub;

fn main() {
    let bots = parse_input!(parser::bots).unwrap();
    println!(
        "Bots in range of strongest: {}",
        bots_in_range_of_strongest(&bots).unwrap()
    );
    println!(
        "Nearest place in range of most bots is {} away",
        most_covered_place(&bots).unwrap()
    )
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Vector {
    x: isize,
    y: isize,
    z: isize,
}

impl Vector {
    fn norm(&self) -> isize {
        self.x.abs() + self.y.abs() + self.z.abs()
    }

    fn dist(&self, other: &Self) -> isize {
        (self - other).norm()
    }

    fn major(&self) -> (position::Index, isize) {
        use position::Index::*;
        [X, Y, Z]
            .iter()
            .map(|&i| (i, i.get(self)))
            .max_by_key(|(_, v)| *v)
            .unwrap()
    }
}

impl Sub for &Vector {
    type Output = Vector;

    fn sub(self, other: &Vector) -> Vector {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

mod position {
    use super::Vector;
    use Index::*;

    #[rustfmt::skip]
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub(super) enum Index {X, Y, Z}

    impl Index {
        pub(super) fn get(self, p: &Vector) -> isize {
            match self {
                X => p.x,
                Y => p.y,
                Z => p.z,
            }
        }

        pub(super) fn update(self, p: Vector, v: isize) -> Vector {
            match self {
                X => Vector { x: v, ..p },
                Y => Vector { y: v, ..p },
                Z => Vector { z: v, ..p },
            }
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_get() {
            let v = Vector { x: 1, y: 3, z: 5 };
            assert_eq!(1, X.get(&v));
            assert_eq!(3, Y.get(&v));
            assert_eq!(5, Z.get(&v));
        }

        #[test]
        fn test_update() {
            let v = Vector { x: 1, y: 3, z: 5 };

            assert_eq!(Vector { x: 77, ..v }, X.update(v.clone(), 77));
            assert_eq!(Vector { y: 938, ..v }, Y.update(v.clone(), 938));
            assert_eq!(Vector { z: -2, ..v }, Z.update(v.clone(), -2));
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Box(Vector, Vector);

impl Box {
    fn split_major(self) -> (Box, Box) {
        let (index, delta) = (&self.1 - &self.0).major();
        let mid = index.get(&self.0) + delta / 2;
        let m0 = index.update(self.1.clone(), mid);
        let m1 = index.update(self.0.clone(), mid + 1);
        (Box(self.0, m0), Box(m1, self.1))
    }

    fn overlaps_range(&self, bot: &Nanobot) -> bool {
        fn reference_bound(bot: isize, min: isize, max: isize) -> isize {
            if bot < min {
                min
            } else if bot > max {
                max
            } else {
                bot
            }
        }

        bot.is_in_range(&Vector {
            x: reference_bound(bot.pos.x, self.0.x, self.1.x),
            y: reference_bound(bot.pos.y, self.0.y, self.1.y),
            z: reference_bound(bot.pos.z, self.0.z, self.1.z),
        })
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Nanobot {
    pos: Vector,
    r: isize,
}

impl Nanobot {
    fn is_in_range(&self, p: &Vector) -> bool {
        self.pos.dist(p) <= self.r
    }
}

fn strongest_nanobot<'a, I>(nanobots: I) -> Option<&'a Nanobot>
where
    I: IntoIterator<Item = &'a Nanobot>,
{
    nanobots.into_iter().max_by_key(|n| n.r)
}

fn bots_in_range_of_strongest(bots: &[Nanobot]) -> Option<usize> {
    let strongest = strongest_nanobot(bots)?;
    Some(bots.iter().filter(|b| strongest.is_in_range(&b.pos)).count())
}

struct CoverageSearchCell {
    cell: Box,
    upper_bound: usize,
}

impl Eq for CoverageSearchCell {}

impl PartialEq for CoverageSearchCell {
    fn eq(&self, other: &Self) -> bool {
        self.upper_bound == other.upper_bound
    }
}

impl Ord for CoverageSearchCell {
    fn cmp(&self, other: &Self) -> Ordering {
        self.upper_bound.cmp(&other.upper_bound)
    }
}

impl PartialOrd for CoverageSearchCell {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl CoverageSearchCell {
    fn new(cell: Box, bots: &[Nanobot]) -> CoverageSearchCell {
        let upper_bound = bots.iter().filter(|b| cell.overlaps_range(b)).count();
        CoverageSearchCell { cell, upper_bound }
    }

    fn split(self, bots: &[Nanobot]) -> Vec<CoverageSearchCell> {
        let (b1, b2) = self.cell.split_major();
        vec![
            CoverageSearchCell::new(b1, bots),
            CoverageSearchCell::new(b2, bots),
        ]
    }

    fn is_atomic(&self) -> bool {
        self.cell.0 == self.cell.1
    }
}

struct CoverageSearch<'a> {
    cells: BinaryHeap<CoverageSearchCell>,
    max_targets_in_range: usize,
    bots: &'a [Nanobot],
}

impl<'a> CoverageSearch<'a> {
    fn start(bots: &[Nanobot]) -> Option<CoverageSearch> {
        let mut cells = BinaryHeap::new();
        cells.push(CoverageSearchCell::new(bounding_box(bots)?, bots));

        while !cells.peek().unwrap().is_atomic() {
            let b = cells.pop().unwrap();
            cells.extend(b.split(bots));
        }

        let max_targets_in_range = cells.peek().unwrap().upper_bound;

        Some(CoverageSearch {
            cells,
            max_targets_in_range,
            bots,
        })
    }

    fn find_closest(&mut self) -> isize {
        let mut distance = std::isize::MAX;

        while (!self.cells.is_empty())
            && self.cells.peek().unwrap().upper_bound == self.max_targets_in_range
        {
            let b = self.cells.pop().unwrap();

            if b.is_atomic() {
                distance = distance.min(b.cell.0.norm());
            } else {
                self.cells.extend(b.split(self.bots));
            }
        }

        distance
    }
}

fn most_covered_place(bots: &[Nanobot]) -> Option<isize> {
    Some(CoverageSearch::start(bots)?.find_closest())
}

fn bounding_box(bots: &[Nanobot]) -> Option<Box> {
    use position::Index;
    use position::Index::*;

    fn bounds(bots: &[Nanobot], i: Index) -> Option<(isize, isize)> {
        use itertools::MinMaxResult::*;
        match bots.iter().map(|b| i.get(&b.pos)).minmax() {
            NoElements => None,
            OneElement(m) => Some((m, m)),
            MinMax(min, max) => Some((min, max)),
        }
    }

    let (x0, x1) = bounds(bots, X)?;
    let (y0, y1) = bounds(bots, Y)?;
    let (z0, z1) = bounds(bots, Z)?;

    Some(Box(
        Vector { x: x0, y: y0, z: z0 },
        Vector { x: x1, y: y1, z: z1 },
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use nom::types::CompleteStr;

    #[test]
    fn test_bounding_box() {
        let bots = [
            Nanobot {
                pos: Vector { x: -4, y: 5, z: 218 },
                r: 5,
            },
            Nanobot {
                pos: Vector { x: 18, y: -5, z: 4 },
                r: 6,
            },
            Nanobot {
                pos: Vector { x: 37, y: 12, z: 50 },
                r: 28,
            },
        ];

        assert_eq!(
            Box(Vector { x: -4, y: -5, z: 4 }, Vector { x: 37, y: 12, z: 218 },),
            bounding_box(&bots).unwrap()
        );
    }

    #[test]
    fn test_split_when_even_size() {
        let b = Box(Vector { x: 0, y: 10, z: 20 }, Vector { x: 20, y: 30, z: 59 });

        assert_eq!(
            (
                Box(Vector { x: 0, y: 10, z: 20 }, Vector { x: 20, y: 30, z: 39 }),
                Box(Vector { x: 0, y: 10, z: 40 }, Vector { x: 20, y: 30, z: 59 })
            ),
            b.split_major()
        );
    }

    #[test]
    fn test_split_when_odd_size() {
        let b = Box(Vector { x: 0, y: 10, z: 20 }, Vector { x: 100, y: 30, z: 60 });

        assert_eq!(
            (
                Box(Vector { x: 0, y: 10, z: 20 }, Vector { x: 50, y: 30, z: 60 }),
                Box(Vector { x: 51, y: 10, z: 20 }, Vector { x: 100, y: 30, z: 60 })
            ),
            b.split_major()
        );
    }

    #[test]
    fn test_major_axis() {
        use position::Index::*;

        assert_eq!((X, 5), Vector { x: 5, y: 2, z: 3 }.major());
        assert_eq!((Y, 18), Vector { x: 5, y: 18, z: 3 }.major());
        assert_eq!((Z, 99), Vector { x: 5, y: 2, z: 99 }.major());
    }

    #[test]
    fn test_vector_subtraction() {
        assert_eq!(
            Vector { x: 5, y: 4, z: 3 },
            &Vector {
                x: 15,
                y: 22,
                z: -900
            } - &Vector {
                x: 10,
                y: 18,
                z: -903
            }
        );
    }

    #[test]
    fn test_if_a_bots_range_intersects_with_a_box() {
        let b = Box(Vector { x: 1, y: 2, z: 3 }, Vector { x: 5, y: 6, z: 7 });

        // Covers whole box
        assert!(b.overlaps_range(&Nanobot {
            r: 3,
            pos: Vector { x: 1, y: 2, z: 3 }
        }));

        // On corner, does not cover whole box
        assert!(b.overlaps_range(&Nanobot {
            r: 1,
            pos: Vector { x: 1, y: 2, z: 3 }
        }));

        // Inside box, does not cover any corner
        assert!(b.overlaps_range(&Nanobot {
            r: 1,
            pos: Vector { x: 3, y: 4, z: 5 }
        }));

        // Outside box on all axes, overlaps
        assert!(b.overlaps_range(&Nanobot {
            r: 3,
            pos: Vector { x: 0, y: 1, z: 2 }
        }));

        // ‘Inside’ box on two axes, too far on third to overlap
        assert!(!b.overlaps_range(&Nanobot {
            r: 3,
            pos: Vector { x: 2, y: 4, z: 18 }
        }));
    }

    #[test]
    fn test_strongest_nanobot() {
        let n1 = Nanobot {
            pos: Vector { x: 1, y: 2, z: 3 },
            r: 1,
        };
        let n2 = Nanobot {
            pos: Vector { x: 5, y: 1, z: 8 },
            r: 3,
        };
        assert_eq!(&n2, strongest_nanobot(vec![&n1, &n2]).unwrap());
    }

    #[test]
    fn test_dist() {
        let p1 = Vector { x: 1, y: 2, z: 3 };
        let p2 = Vector { x: 4, y: 0, z: 3 };
        let (dx, dy, dz) = (3, 2, 0);

        assert_eq!(dx + dy + dz, p1.dist(&p2));
        assert_eq!(dx + dy + dz, p2.dist(&p1));
    }

    #[test]
    fn test_bots_in_range_of_strongest() {
        let bots = parser::bots(CompleteStr(indoc!(
            "
            pos=<0,0,0>, r=4
            pos=<1,0,0>, r=1
            pos=<4,0,0>, r=3
            pos=<0,2,0>, r=1
            pos=<0,5,0>, r=3
            pos=<0,0,3>, r=1
            pos=<1,1,1>, r=1
            pos=<1,1,2>, r=1
            pos=<1,3,1>, r=1
            "
        )))
        .unwrap()
        .1;
        assert_eq!(7, bots_in_range_of_strongest(&bots).unwrap());
    }

    fn part_2_bots() -> Vec<Nanobot> {
        parser::bots(CompleteStr(indoc!(
            "
            pos=<10,12,12>, r=2
            pos=<12,14,12>, r=2
            pos=<16,12,12>, r=4
            pos=<14,14,14>, r=6
            pos=<50,50,50>, r=200
            pos=<10,10,10>, r=5
            "
        )))
        .unwrap()
        .1
    }

    #[test]
    fn test_greatest_achievable_coverage() {
        let bots = part_2_bots();
        let s = CoverageSearch::start(&bots).unwrap();
        assert_eq!(5, s.max_targets_in_range);
    }

    #[test]
    fn test_distance_of_closest_point_of_greatest_coverage() {
        let bots = part_2_bots();
        assert_eq!(36, most_covered_place(&bots).unwrap());
    }
}

mod parser {
    use super::{Nanobot, Vector};
    use commons::parser::int;
    use nom::types::CompleteStr;
    use nom::{char, do_parse, eol, many1, named, tag, terminated, IResult};

    named!(position<CompleteStr, Vector>, do_parse!(
           char!('<') >>
        x: int        >>
           char!(',') >>
        y: int        >>
           char!(',') >>
        z: int        >>
           char!('>') >>
        (Vector{x, y, z})
    ));

    named!(bot<CompleteStr, Nanobot>, do_parse!(
             tag!("pos=") >>
        pos: position     >>
             tag!(", r=") >>
        r:   int          >>
        (Nanobot{pos, r})
    ));

    pub(super) fn bots(i: CompleteStr) -> IResult<CompleteStr, Vec<Nanobot>> {
        many1!(i, terminated!(bot, eol))
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        fn parse<R>(p: fn(CompleteStr) -> IResult<CompleteStr, R>, s: &str) -> R {
            match p(CompleteStr(s)) {
                Ok(r) => r.1,
                Err(r) => panic!("{:?}", r),
            }
        }

        #[test]
        fn test_parse_nanobot() {
            let bot = parse(bot, "pos=<7,-3,4>, r=2");
            let pos = Vector { x: 7, y: -3, z: 4 };
            let r = 2;
            assert_eq!(Nanobot { pos, r }, bot);
        }

        #[test]
        fn test_parse_input() {
            let bots = parse(
                bots,
                indoc!(
                    "
                pos=<7,3,4>, r=2
                pos=<18,8,89>, r=33
                "
                ),
            );

            assert_eq!(
                vec![
                    Nanobot {
                        pos: Vector { x: 7, y: 3, z: 4 },
                        r: 2
                    },
                    Nanobot {
                        pos: Vector { x: 18, y: 8, z: 89 },
                        r: 33
                    },
                ],
                bots
            );
        }
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref BOTS: Vec<Nanobot> = { parse_input!(parser::bots).unwrap() };
    }

    #[test]
    fn test_part_1() {
        assert_eq!(219, bots_in_range_of_strongest(&*BOTS).unwrap());
    }

    #[test]
    fn test_part_2() {
        assert_eq!(83779034, most_covered_place(&*BOTS).unwrap());
    }
}
