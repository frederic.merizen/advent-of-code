use commons::{lines, parse_input, Result};
use std::collections::{HashMap, HashSet};
use std::fmt;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct ClaimId(u64);

impl fmt::Display for ClaimId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Claim {
    id: ClaimId,
    l: u64,
    t: u64,
    w: u64,
    h: u64,
}

mod parser {
    use super::{Claim, ClaimId};
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{char, do_parse, tag, IResult};

    #[rustfmt::skip]
    pub(super) fn claim(i: CompleteStr) -> IResult<CompleteStr, Claim> {
        do_parse!(i,
                char!('#')  >>
            id: uint        >>
                tag!(" @ ") >>
            l:  uint        >>
                char!(',')  >>
            t:  uint        >>
                tag!(": ")  >>
            w:  uint        >>
                char!('x')  >>
            h:  uint        >>
            (Claim{ id: ClaimId(id), l, t, w, h})
        )
    }
}

fn read_input<'a>() -> Result<'a, Vec<Claim>> {
    parse_input!(lines!(parser::claim))
}

type ClaimMap = HashMap<(u64, u64), HashSet<ClaimId>>;

fn map_claims(claims: &[Claim]) -> ClaimMap {
    let mut map = HashMap::new();

    for c in claims {
        for i in c.l..(c.l + c.w) {
            for j in c.t..(c.t + c.h) {
                map.entry((i, j)).or_insert_with(HashSet::new).insert(c.id);
            }
        }
    }

    map
}

fn overlaps(claims: &ClaimMap) -> usize {
    claims.values().filter(|cs| cs.len() > 1).count()
}

fn non_overlapping(claims: &ClaimMap) -> Option<ClaimId> {
    let mut maybe = HashSet::new();
    let mut definitely_not = HashSet::new();

    for cs in claims.values() {
        let target = if cs.len() == 1 {
            &mut maybe
        } else {
            &mut definitely_not
        };
        for c in cs {
            target.insert(c.clone());
        }
    }

    maybe.into_iter().find(|c| !definitely_not.contains(c))
}

fn main() {
    let claims = map_claims(&read_input().unwrap());
    println!("Overlaps: {}", overlaps(&claims));
    println!("Non-overlapping: {}", non_overlapping(&claims).unwrap());
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref CLAIMS: ClaimMap = map_claims(&read_input().unwrap());
    }

    #[test]
    fn test_overlaps() {
        assert_eq!(112_378, overlaps(&*CLAIMS));
    }

    #[test]
    fn test_non_overlapping() {
        assert_eq!(ClaimId(603), non_overlapping(&*CLAIMS).unwrap());
    }
}
