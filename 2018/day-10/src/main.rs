use commons::{lines, parse_input, Result};
use itertools::Itertools;
use itertools::MinMaxResult::MinMax;
use std::collections::HashSet;

#[derive(Hash, PartialEq, Eq, Clone)]
struct Vector {
    x: i32,
    y: i32,
}

struct Star {
    position: Vector,
    velocity: Vector,
}

impl Star {
    fn travel(self) -> Self {
        let velocity = self.velocity;
        let x = self.position.x + velocity.x;
        let y = self.position.y + velocity.y;
        Star {
            position: Vector { x, y },
            velocity,
        }
    }
}

mod parser {
    use super::{Star, Vector};
    use commons::parser::int;
    use nom::types::CompleteStr;
    use nom::{char, do_parse, named, tag, IResult};

    named!(vector<CompleteStr, Vector>,
        do_parse!(
               tag!("< ") >>
            x: int        >>
               tag!(", ") >>
            y: int        >>
               char!('>') >>
            (Vector{ x, y })
        )
    );

    #[rustfmt::skip]
    pub(super) fn star(i: CompleteStr) -> IResult<CompleteStr, Star> {
        do_parse!(i,
                      tag!("position=")  >>
            position: vector             >>
                      tag!(" velocity=") >>
            velocity: vector             >>
            (Star { position, velocity })
        )
    }
}

fn read_input<'a>() -> Result<'a, Vec<Star>> {
    parse_input!(lines!(parser::star))
}

fn bounds(stars: &[Star]) -> Option<(Vector, Vector)> {
    if let MinMax(y_min, y_max) = stars.iter().map(|s| s.position.y).minmax() {
        if let MinMax(x_min, x_max) = stars.iter().map(|s| s.position.x).minmax() {
            Some((Vector { x: x_min, y: y_min }, Vector { x: x_max, y: y_max }))
        } else {
            None
        }
    } else {
        None
    }
}

fn height(stars: &[Star]) -> Option<i32> {
    let (upper_left, lower_right) = bounds(stars)?;
    Some(lower_right.y - upper_left.y)
}

fn message(mut stars: Vec<Star>) -> (u32, Vec<Star>) {
    let mut i = 0;
    loop {
        i += 1;
        stars = stars.into_iter().map(|s| s.travel()).collect();
        match height(&stars) {
            Some(h) if h <= 15 => {
                return (i, stars);
            }
            _ => (),
        }
    }
}

fn format_message(stars: Vec<Star>) -> Option<String> {
    let (upper_left, lower_right) = bounds(&stars)?;
    let positions: HashSet<_> = stars.into_iter().map(|s| s.position).collect();
    Some(
        (upper_left.y..=lower_right.y)
            .map(|y| {
                (upper_left.x..=lower_right.x)
                    .map(|x| {
                        if positions.contains(&Vector { x, y }) {
                            "█"
                        } else {
                            " "
                        }
                    })
                    .collect::<String>()
            })
            .join("\n"),
    )
}

fn main() {
    let input = read_input().unwrap();
    let (i, message) = message(input);
    let s = format_message(message).unwrap();
    println!("{}", s);
    println!("{}", i);
}
