(ns day-03
  (:require
    [better-cond.core :refer [defnc]]
    [clojure.java.io :as io]
    [com.rpl.specter :refer :all]))

(defnc read-diagnostic-code [s]
  :let [code (Integer/parseInt s 2)]
  (map
    (fn [c pos]
      {:value    (Character/getNumericValue ^char c)
       :position pos
       :code     code})
    s
    (range (dec (count s)) -1 -1)))

(def input
  (with-open [rdr (io/reader "resources/day-03.txt")]
    (doall (mapcat read-diagnostic-code (line-seq rdr)))))

(defn flatten-freqs [freqs]
  (for [[pos pos-freqs] freqs
        [val val-pos-freq] pos-freqs]
    {:position pos, :value val, :frequency val-pos-freq}))

(defn statistics [codes]
  (->> codes
       (group-by :position)
       (transform [MAP-VALS] #(frequencies (map :value %)))
       flatten-freqs))

(def statistics-over-all-codes (statistics input))

(defn preview [statistics]
  {:data     {:values statistics}
   :encoding {:x       {:field :position, :type :ordinal, :scale {:reverse true}}
              :xOffset {:field :value}
              :color   {:field :value}
              :y       {:field :frequency, :type :quantitative}}
   :mark     :bar})

(defn composite [extr-key]
  (->> statistics-over-all-codes
       (group-by :position)
       vals
       (map (fn [freqs]
              (let [{:keys [value position]} (apply extr-key :frequency freqs)]
                (bit-shift-left value position))))
       (apply +)))

(def gamma (composite max-key))
(def epsilon (composite min-key))

(def leftmost-pos (->> statistics-over-all-codes (map :position) (apply max)))
(def all-positions (range leftmost-pos -1 -1))

(defnc o2-criterion [[{val-1 :value, freq-1 :frequency :or {freq-1 0}}
                      {val-2 :value, freq-2 :frequency :or {freq-2 0}}]]
  (= freq-1 freq-2) 1
  (> freq-1 freq-2) val-1
  val-2)

(defnc filter-at-position [criterion numbers position]
  :let [digits (->> numbers
                    (keep #(when (= position (:position %))
                             (:value %)))
                    frequencies
                    (map (fn [[value frequency]] {:value value, :frequency frequency})))
        target (criterion digits)
        kept-codes (into #{}
                         (keep #(when (= [position target] [(:position %) (:value %)])
                                  (:code %)))
                         numbers)]

  (= 1 (count kept-codes)) (reduced {:code (first kept-codes)})
  (filter #(kept-codes (:code %)) numbers))

(def o2-candidates
  (reductions #(filter-at-position o2-criterion %1 %2)
              input
              all-positions))

(defnc filtering-stats [candidates]
  :let [data (map
               (fn [pos candidates-at-step]
                 {:position pos
                  :count    (count (into #{} (map :code candidates-at-step)))})
               (cons (inc leftmost-pos) all-positions)
               candidates)]
  {:data     {:values data}
   :encoding {:x {:field :position, :type :quantitative, :scale {:reverse true}}
              :y {:field :count, :type :quantitative, :scale {:type :log}}}
   :mark     :line})

(defn statistics-per-digit [candidates]
  (mapcat
    (fn [pos candidates-at-step]
      (filter #(= pos (:position %))
              (statistics candidates-at-step)))
    all-positions
    candidates))

(def o2-rating (:code (last o2-candidates)))

(defn co2-criterion [digits]
  (- 1 (o2-criterion digits)))

(def co2-candidates
  (reductions #(filter-at-position co2-criterion %1 %2)
              input
              all-positions))

(def co2-rating (:code (last co2-candidates)))

(def report
  [:div
   [:markdown "
# Advent of code 2021, day 3
## part 1
[Instructions](https://adventofcode.com/2021/day/3)

Computing the power consumption of the ship.

Let’s start by plotting the frequency of each digit in each position.
"]
   [:vega-lite (preview statistics-over-all-codes)]
   [:markdown (str "
Gamma is made of the most frequent bits: " (Integer/toBinaryString gamma) "  " "
Epsilon is made of the least frequent bits: " (Integer/toBinaryString epsilon) "

The power consumption is **" (* gamma epsilon) "**

## part 2
[Instructions](https://adventofcode.com/2021/day/3#part2)

Now let’s find the oxygen generator rating, by filtering down numbers right to left, only keeping numbers with the most
frequent bit.

Number of candidates for O2 generator rating, by bit position")]
   [:vega-lite (filtering-stats o2-candidates)]
   [:markdown "Bit count by position for search of O2 generator rating"]
   [:vega-lite (-> (preview (statistics-per-digit o2-candidates))
                   (assoc-in [:encoding :y :scale :type] :log)
                   (assoc-in [:encoding :y :stack] false))]
   [:markdown (str "
O2 rating is " (Integer/toBinaryString o2-rating) "  " "
Number of candidates for CO2 scrubber rating, by bit position")]
   [:vega-lite (filtering-stats co2-candidates)]
   [:markdown "Bit count by position for search of CO2 scrubber rating"]
   [:vega-lite (-> (preview (statistics-per-digit co2-candidates))
                   (assoc-in [:encoding :y :scale :type] :log)
                   (assoc-in [:encoding :y :stack] false))]
   [:markdown (str "
CO2 rating is " (Integer/toBinaryString co2-rating) "  " "
Life support rating is **" (* o2-rating co2-rating) "**")]])
