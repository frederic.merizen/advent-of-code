(ns day-04
  (:require
    [better-cond.core :refer [defnc]]
    [clojure.java.io :as io]
    [clojure.string :as string]
    [com.rpl.specter :refer :all]))

(defn transpose [matrix]
  (apply map list matrix))

(defn add-columns [lines]
  (concat lines (transpose lines)))

(defn rows-by-draw [rows]
  (->> rows
       (mapcat (fn [i row] (for [n row] {:index i, :number n})) (range))
       (group-by :number)
       (transform [MAP-VALS ALL] :index)))

(defnc board-for-play [lines]
  :let [rows (mapv set (add-columns lines))]
  {:rows rows
   :by-draw (rows-by-draw rows)})

(def input
  (with-open [rdr (io/reader "resources/day-04.txt")]
    (let [[draws & boards] (line-seq rdr)
          draws (map #(Integer/parseInt %) (string/split draws #","))
          boards (->> boards
                      (partition-by empty?)
                      (remove #(= 1 (count %)))
                      (map (fn [lines]
                             (for [l lines]
                               (for [n (-> l string/trim (string/split #" +"))]
                                 (Integer/parseInt n))))))]
      {:draws draws, :boards (mapv board-for-play boards)})))

(defnc play-round [{:keys [rows by-draw] :as board} draw]
  :let [row-nums (get by-draw draw)]

  (not row-nums)
  board

  :let [{:keys [rows] :as board} (multi-transform (multi-path [:rows (apply multi-path row-nums) (terminal #(disj % draw))]
                                                              [:by-draw draw (terminal-val NONE)])
                                                  board)]

  (some #(-> rows (get %) empty?) row-nums)
  (assoc board :complete true)

  board)

(defn play-all [{:keys [draws boards]}]
  (reductions (fn [boards draw]
                (for [board boards
                      :when (not (:complete board))]
                  (-> board
                      (play-round draw)
                      (assoc :draw draw))))
              boards
              draws))

(defn completed-boards [input]
  (->> input
       play-all
       (mapcat #(filter :complete %))))

(defn score [{:keys [draw by-draw]}]
  (* draw (apply + (keys by-draw))))

(defn part-1 []
  (score (first (completed-boards input))))

(defn part-2 []
  (score (last (completed-boards input))))

(defn preview [statistics]
  {:data     {:values statistics}
   :encoding {:x       {:field :position, :type :ordinal, :scale {:reverse true}}
              :xOffset {:field :value}
              :color   {:field :value}
              :y       {:field :frequency, :type :quantitative}}
   :mark     :bar})


(def report
  [:div
   [:markdown "
# Advent of code 2021, day 4
## part 1
[Instructions](https://adventofcode.com/2021/day/4)

Playing bingo
"]])
