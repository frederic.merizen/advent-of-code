(ns day-02
  (:require
    [clojure.java.io :as io]
    [clojure.string :as string]))

(defn read-command [s]
  (let [[cmd amt] (string/split s #" ")]
    {:command (keyword cmd), :amount (read-string amt)}))

(def input
  (with-open [rdr (io/reader "resources/day-02.txt")]
    (doall (map read-command (line-seq rdr)))))

(defn move [moves position {:keys [command amount]}]
  ((moves command) position amount))

(defn positions [{:keys [initial-position moves]}]
  (into [] (reductions #(move moves %1 %2) initial-position input)))

(defn preview [data]
  {:data     {:values data}
   :encoding {:x {:field :horizontal, :type :quantitative}
              :y {:field :depth, :type :quantitative, :scale {:reverse true}}}
   :mark     :line
   :height   500
   :width    1000})

(defn ship-journey [options]
  (let [positions (positions options)
        end-point (peek positions)]
    (list
     [:vega-lite (preview positions)]
     [:markdown (str "
The final horizontal position is " (:horizontal end-point) "  " "
The final depth is " (:depth end-point) "  " "
The answer to the puzzle is **" (* (:depth end-point) (:horizontal end-point)) "**
")])))

(def report
  [:div
   [:markdown "
# Advent of code 2021, day 2
## part 1
[Instructions](https://adventofcode.com/2021/day/2)

Let’s track a submarine through a series of up, down and forward moves
"]
   (ship-journey {:initial-position {:depth 0, :horizontal 0}
                  :moves            {:up      #(update %1 :depth - %2)
                                     :down    #(update %1 :depth + %2)
                                     :forward #(update %1 :horizontal + %2)}})
   [:markdown "
## part 2
[Instructions](https://adventofcode.com/2021/day/2#part2)

This time, the up and down command change the aim of the ship
"]
   (ship-journey {:initial-position {:depth 0, :horizontal 0, :aim 0}
                  :moves            {:up      #(update %1 :aim - %2)
                                     :down    #(update %1 :aim + %2)
                                     :forward #(-> %1
                                                   (update :horizontal + %2)
                                                   (update :depth + (* %2 (:aim %1))))}})])
