(ns day-01
  (:require
    [clojure.java.io :as io]))

(def input
  (with-open [rdr (io/reader "resources/day-01.txt")]
    (doall (map read-string (line-seq rdr)))))

(defn ->data [input]
  (map
    (fn [idx prev cur]
      {:index (inc idx), :prev prev, :depth cur, :delta (when prev (- cur prev))})
    (range)
    (concat [nil] input)
    input))

(def part-1-data (->data input))

(def green "#06982d")
(def red "#ae1325")
(defn preview [data]
  {:data     {:values data}
   :encoding {:x     {:field :index, :type :quantitative, :scale {:domain [0 (count data)]}}
              :y     {:field :depth, :type :quantitative}
              :color {:condition {:test "datum.delta < 0", :value red}
                      :value     green}}
   :mark     "bar"
   :height   500
   :width    2500})

(defn increasing-depth-count [data]
  (count (filter #(pos-int? (:delta %)) data)))

(def part-2-data (->data (map #(apply + %) (partition 3 1 input))))

(def report
  [:div
   [:markdown "
# Advent of code 2021, day 1
## part 1
[Instructions](https://adventofcode.com/2021/day/1)

We are given a list of depths. We must count the depths that are greater than the one coming immediately before it.

Let’s take a look at the data
"]
   [:vega-lite (preview part-1-data)]
   [:markdown (str "
In total, there are **" (increasing-depth-count part-1-data) "** depths that are greater than their predecessor

## part 2
[Instructions](https://adventofcode.com/2021/day/1#part2)

This time, we process depth as a 3 point sliding measure
")]
   [:vega-lite (preview part-2-data)]
   [:markdown (str "This time, there are **" (increasing-depth-count part-2-data) "** depths that are greater than their predecessor")]])
