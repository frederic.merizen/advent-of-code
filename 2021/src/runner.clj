(ns runner
  (:require
    [oz.core :as oz]
    [oz.live]))

(defn live-view [day]
  (oz.live/watch! (str "src/day_" day ".clj")
                  (fn [event]
                    (oz.live/reload-file! event)
                    (oz/view! @(requiring-resolve (symbol (str "day-" day) "report"))))))

(defn render [day]
  (oz/export! @(requiring-resolve (symbol (str "day-" day) "report"))
              (str "target/day-" day ".html")))

(comment
  (live-view "03")
  (render "03"))