(ns util.explore-test
  (:require [clojure.test :refer [deftest is testing]]
            [util.explore :refer :all]))

(defn view-open-list [l] (select-keys l [:priority :meta]))

(deftest test-enqueue
  (is (= {:priority {:a 3}, :meta {:a {:cost 1, :pos :a, :parent :b}}}
         (view-open-list (enqueue (open-list (constantly 2)) {:cost 1, :pos :a, :parent :b})))))

(deftest test-open-list
  (is (= {:priority {:a 3}, :meta {:a {:cost 1, :pos :a, :parent :b}}}
         (view-open-list (open-list (constantly 2) {:cost 1 :pos :a, :parent :b})))))

(deftest test-dequeue
  (let [[queue elem] (dequeue (open-list (constantly 2)
                                         {:cost 1 :pos :a}
                                         {:cost 2 :pos :b}))]
    (is (= :a elem))
    (is (= (view-open-list (open-list (constantly 2) {:cost 2 :pos :b}))
           (view-open-list queue)))))

(deftest test-has-cheaper-option?
  (testing "when there is no other option"
    (is (not (has-cheaper-option? {} {:cost 1}))))
  (testing "when the other option is cheaper"
    (is (has-cheaper-option? {1 {:cost 1}}
                             {:cost 2 :pos 1})))
  (testing "when the other option is more expensive"
    (is (not (has-cheaper-option? {1 {:cost 2}}
                                  {:cost 1 :pos 1}))))
  (testing "ignore unrelated positions"
    (is (not (has-cheaper-option? {1 {:cost 1}}
                                  {:cost 2 :pos 2})))))

(def initial [0 0])
(def goal [5 5])

(defn neighbours [[x y]]
  (let [dest (fn [x y] {:pos [x y]})]
    [(dest (inc x) y) (dest (dec x) y) (dest x (inc y)) (dest x (dec y))]))

(defn goal? [pos] (= pos goal))

(defn dist [a b]
  (apply + (map #(Math/abs (- %2 %1)) a b)))

(defn heuristic [pos]
  (dist pos goal))

(defn hop-by-1? [[a b]]
  (= 1 (dist a b)))

(deftest test-initial-state
  (let [is (initial-state (constantly 0) initial)]
    (is (= {}
           (:closed-list is)))
    (is (= {:priority {[0 0] 0}, :meta {[0 0] {:pos [0 0], :cost 0}}}
           (view-open-list (:open-list is))))))

(deftest test-pop-step
  (let [[{:keys [closed-list open-list]} pos meta] (pop-step (initial-state (constantly 0) initial))]
    (is (= {} closed-list))
    (is (= {:priority {}, :meta {}}
           (view-open-list open-list)))
    (is (= initial pos))
    (is (= {:pos initial, :cost 0}
           meta))))

(deftest test-step
  (let [{:keys [closed-list open-list]} (step (initial-state heuristic initial) neighbours (constantly false) identity)]
    (is (= {initial {:cost 0}} closed-list))
    (is (= {:priority {[1 0] 10, [0 1] 10, [-1 0] 12, [0 -1] 12},
            :meta {[1 0] {:pos [1 0], :cost 1, :parent [0 0]},
                   [-1 0] {:pos [-1 0], :cost 1, :parent [0 0]},
                   [0 1] {:pos [0 1], :cost 1, :parent [0 0]},
                   [0 -1] {:pos [0 -1], :cost 1, :parent [0 0]}}}
           (view-open-list open-list)))))

(deftest test-a*
  (testing "when goal is reachable"
    (let [{:keys [cost path]} (a* initial goal? neighbours heuristic)]
      (is (= 10 cost))
      (is (= 11 (count path)))
      (is (every? hop-by-1? (partition 2 1 path)))
      (is (= goal (first path)))
      (is (= initial (last path)))))

  (testing "when goal is unreachable"
    (let [neighbours (fn [pos]
                       (filter #(< (dist (:pos %) [0 0]) 3) (neighbours pos)))]
      (is nil? (a* initial goal? neighbours heuristic)))))

(deftest test-reachable-goals
  (testing "list all reachable goals"
    (let [neighbours (fn [pos]
                       (filter #(< (dist (:pos %) [0 0]) 3) (neighbours pos)))
          reached (last (reachable-goals initial #(= 1 (dist % [0 0])) neighbours heuristic))]
      (is (= #{[1 0] [-1 0] [0 1] [0 -1]}
             (set (keys reached))))
      (is (= [1 1 1 1]
             (map :cost (vals reached)))))))
