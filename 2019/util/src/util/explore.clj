(ns util.explore
  (:require
   [clojure.data.priority-map :refer [priority-map]]))

(defn has-cheaper-option? [meta {:keys [pos cost]}]
  (when-let [existing (get-in meta [pos :cost])]
    (<= existing cost)))

(defn enqueue [{:keys [heuristic meta] :as open-list} {:keys [cost pos] :as option}]
  (cond-> open-list
    (not (has-cheaper-option? meta option)) (-> (update :priority assoc pos (+ cost (heuristic pos)))
                                                (update :meta assoc pos option))))

(defn open-list [heuristic & options]
  (reduce enqueue
          {:heuristic heuristic
           :priority (priority-map)
           :meta {}}
          options))

(defn dequeue [open-list]
  (when-let [[pos] (first (:priority open-list))]
    [(-> open-list
         (update :priority dissoc pos)
         (update :meta dissoc pos))
     pos
     (get-in open-list [:meta pos])]))

(defn set-total-cost [base-cost]
  (map #(update % :cost (fnil + 1) base-cost)))

(defn keep-cheaper-options [closed-list]
  (filter #(not (has-cheaper-option? closed-list %))))

(defn set-parent [parent]
  (map #(assoc % :parent parent)))

(defn add-neighbours [state pos meta neighbours]
  (-> state
      (update :open-list #(transduce (comp (set-total-cost (:cost meta))
                                           (keep-cheaper-options (:closed-list state))
                                           (set-parent pos))
                                     (completing enqueue)
                                     %
                                     (neighbours pos)))
      (update :closed-list assoc pos (select-keys meta [:cost :parent]))))

(defn path-to [closed-list pos]
  (take-while identity (iterate #(get-in closed-list [% :parent]) pos)))

(defn pop-step [state]
  (when-let [result (dequeue (:open-list state))]
    (update result 0 #(assoc state :open-list %))))

(defn step [state neighbours goal? found]
  (when-let [[state pos meta] (pop-step state)]
    (cond-> state
      (not (has-cheaper-option? (:closed-list state) meta)) (add-neighbours pos meta neighbours)
      (goal? pos) (found pos meta))))

(defn initial-state [heuristic pos]
  {:open-list (open-list heuristic {:pos pos, :cost 0})
   :closed-list {}})

(defn set-found [state pos meta]
  (assoc state :found {:cost (:cost meta)
                       :path (path-to (:closed-list state) pos)}))

(defn a*
  ([initial goal? neighbours]
   (a* initial goal? neighbours (constantly 0)))
  ([initial goal? neighbours heuristic]
   (:found (first (drop-while #(and % (not (:found %)))
                              (iterate #(step % neighbours goal? set-found)
                                       (initial-state heuristic initial)))))))

(defn add-found [state pos meta]
  (assoc-in state [:found pos] {:cost (:cost meta)
                                :path (path-to (:closed-list state) pos)}))

(defn reachable-goals
  ([initial goal? neighbours]
   (reachable-goals initial goal? neighbours (constantly 0)))
  ([initial goal? neighbours heuristic]
   (map :found (take-while identity
                           (iterate #(step % neighbours goal? add-found)
                                    (initial-state heuristic initial))))))
