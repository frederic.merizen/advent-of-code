(ns day-18-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-18 :refer :all]
   [util.map :as map]))

(def parsed-map
  (map/parse [".###."
              "#A@a#"
              ".###."]
             parse-options))

(deftest test-parse
  (is (= {               [1 0] [:wall],    [2 0] [:wall],    [3 0] [:wall]
          [0 1] [:wall], [1 1] [:door \A], [2 1] [:robot 0], [3 1] [:key \a], [4 1] [:wall]
                         [1 2] [:wall],    [2 2] [:wall],    [3 2] [:wall]}
         parsed-map)))

(deftest test-tile-type-at
  (is (= :robot (tile-type-at parsed-map [2 1]))))

(deftest test-node?
  (is (node? parsed-map [1 1] [2 1]) "a door is a node")
  (is (node? parsed-map [3 1] [2 1]) "a key is a node")
  (is (not (node? parsed-map [1 0] [2 1])) "a wall is not a node")
  (is (not (node? parsed-map [1 1] [1 1])) "The init tile does not count as a new node"))

(deftest test-neighbours
  (is (empty? (neighbours parsed-map [1 1] [2 1])) "Do not look for new neighbours beyond a new node")
  (is (= #{{:pos [1 1]}, {:pos [3 1]}}
         (set (neighbours parsed-map [2 1] [2 1])))
      "Neighbours are adjacent non-wall tiles"))

(deftest test-distances-from
  (is (= {[:door \A] 1, [:key \a] 1}
         (distances-from parsed-map [2 1]))))

(deftest test-abstract-graph
  (is (= {[:robot 0] {[:door \A] 1, [:key \a] 1}
          [:key \a]     {[:door \A] 2}
          [:door \A]    {[:key \a] 2}}
         (abstract-graph parsed-map))))

(deftest test-into-with
  (is (= {:a 6, :b 2, :c 8}
         (into-with + {:a 1 :b 2} [[:a 5] [:c 8]])))
  (is (= {:a -4, :b 2, :c -8}
         (into-with + {:a 1 :b 2} (map (fn [[k v]] [k (- v)])) {:a 5, :c 8}))))

(deftest test-merge-paths
  (is (= {:a 10, :b 15, :c 72, :d 31}
         (merge-paths {:a 10, :b 15, :c 100, :key 3}
                      :key
                      3
                      {:b 13, :c 69, :d 28}))))

(deftest test-export-to-reachable
  (testing "do not touch innocent bystanders"
    (is (= {:bystander {:a 12}
            :a {:bystander 12}
            :key {:a 5}}
           (export-to-reachable {:bystander {:a 12}
                                 :a {:key 5, :bystander 12}
                                 :key {:a 5}}
                                :key))))

  (testing "create new path"
    (is (= {:a {:b 12}
            :b {:a 12}
            :key {:a 5, :b 7}}
           (export-to-reachable {:a {:key 5}
                                 :b {:key 7}
                                 :key {:a 5, :b 7}}
                                :key))))

  (testing "shorten existing path"
    (is (= {:a {:b 12}
            :b {:a 12}
            :key {:a 5, :b 7}}
           (export-to-reachable {:a {:key 5, :b 20}
                                 :b {:key 7, :a 20}
                                 :key {:a 5, :b 7}}
                                :key))))

  (testing "ignore longer path"
    (is (= {:a {:b 2}
            :b {:a 2}
            :key {:a 5, :b 7}}
           (export-to-reachable {:a {:key 5, :b 2}
                                 :b {:key 7, :a 2}
                                 :key {:a 5, :b 7}}
                                :key)))))

(deftest test-export-to-robots
  (is (= {[:robot 0] {[:key \b] 7}
          [:key \a] {[:key \b] 2}}
         (export-to-robots {[:robot 0] {[:key \a] 5}
                            [:key \a] {[:key \b] 2}}
                           1 [:key \a]))))

(deftest test-grab-key
  (testing "removes the key and the corresponding door"
    (is (= {[:robot 0] {}}
           (grab-key {[:robot 0] {[:door \A] 12, [:key \a] 5}
                      [:key \a] {}
                      [:door \A] {}}
                     1 0 \a))))
  (testing "Connect remaining parts of the graph"
    (is (= {[:robot 0] {[:door \B] 28}
            [:robot 1] {[:door \B] 20}
            [:door \B] {}}
           (grab-key {[:robot 0] {[:key \a] 10}
                      [:robot 1] {[:door \A] 2}
                      [:key \a] {[:door \A] 10}
                      [:door \A] {[:door \B] 18, [:key \a] 10}
                      [:door \B] {[:door \A] 18}}
                     2 0 \a)))))

(deftest test-next-nodes-to-explore
  (is (= [{:pos {[:robot 0] {}}, :cost 1}]
         (next-nodes-to-explore (abstract-graph parsed-map) 1))
      "The next position to explore is the one after having grabbed key a (which also removes door a)"))

(deftest test-cheapest-tour
  (is (= 1 (:cost (cheapest-tour (abstract-graph parsed-map) 1)))))

(deftest test-parse-patched
  (is (= {               [1 0] [:wall],    [2 0] [:wall], [3 0] [:wall],    [4 0] [:wall]
          [0 1] [:wall], [1 1] [:robot 0], [2 1] [:wall], [3 1] [:robot 1], [4 1] [:door \A], [5 1] [:wall]
          [0 2] [:wall], [1 2] [:wall],    [2 2] [:wall], [3 2] [:wall],    [4 2] [:wall],    [5 2] [:wall]
          [0 3] [:wall], [1 3] [:robot 2], [2 3] [:wall], [3 3] [:robot 3], [4 3] [:key \a],  [5 3] [:wall]
                         [1 4] [:wall],    [2 4] [:wall], [3 4] [:wall],    [4 4] [:wall]}
         (map/parse [".####"
                     "#...A#"
                     "#.@.##"
                     "#...a#"
                     ".####."]
                    parse-patched-options))))
