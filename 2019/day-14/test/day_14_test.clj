(ns day-14-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-14 :refer :all]))

(deftest test-parse-input
  (is (= {"TOTO" {:inputs {"BADA" 12, "YADA" 15}, :quantity 2}
          "NANI" {:inputs {"NANA" 333, "ZOSH" 89, "YODA" 25}, :quantity 9}}
         (parse-input "12 BADA, 15 YADA => 2 TOTO\n333 NANA, 89 ZOSH, 25 YODA => 9 NANI\n"))))

(deftest tests-batchcount
  (is (= 1 (batch-count 15 15)))
  (is (= 1 (batch-count 1 15)))
  (is (= 2 (batch-count 17 15))))

(deftest test-scale
  (is (= {"a" 6 "b" 30} (scale 3 {"a" 2 "b" 10}))))

(deftest test-add-batch
  (is (= {"a" 17 "b" 38 "c" 8}
         (add-batch {"a" 17 "b" 31}
                    {"b" 7 "c" 8}))))

(deftest test-stash-ore
  (is (= {:required {"yadda" 15}, :ore 0} (stash-ore {:required {"yadda" 15}})))
  (is (= {:required {"yadda" 15}, :ore 2} (stash-ore {:required {"yadda" 15}, :ore 2})))
  (is (= {:required {"yadda" 15}, :ore 3} (stash-ore {:required {"yadda" 15, "ORE" 3}})))
  (is (= {:required {"yadda" 15}, :ore 7} (stash-ore {:required {"yadda" 15, "ORE" 1}, :ore 6}))))

(deftest test-reduce-requirements
  (is (= {:required {"GAS" 5}}
         (select-keys (reduce-requirements {:required {"FUEL" 2}
                                            :reactions {"FUEL" {:inputs {"GAS" 5} :quantity 2}}})
                      [:required]))
      "Use reactions to simplify requirements")

  (is (= {:required {"GAS" 10}}
         (select-keys (reduce-requirements {:required {"BANANA" 4}
                                            :reactions {"BANANA" {:inputs {"GAS" 5} :quantity 2}}})
                      [:required]))
      "Scale reaction")

  (is (= {:required {"GAS" 12}}
         (select-keys (reduce-requirements {:required {"FUEL" 2, "GAS" 7}
                                            :reactions {"FUEL" {:inputs {"GAS" 5} :quantity 2}}})
                      [:required]))
      "Add to existing requirements")

  (is (= {:required {}, :ore 5}
         (select-keys (reduce-requirements {:required {"FUEL" 2}
                                            :reactions {"FUEL" {:inputs {"ORE" 5} :quantity 2}}})
                      [:required :ore]))
      "ORE is a special requirement")

  (is (= {:available {"BANANA" 1}}
         (select-keys (reduce-requirements {:required {"BANANA" 3}
                                            :reactions {"BANANA" {:inputs {"GAS" 5} :quantity 2}}})
                      [:available]))
      "Stash leftovers for later")

  (is (= {:required {"GAS" 3}, :available {"TEARS" 0}}
         (select-keys (reduce-requirements {:required {"TEARS" 2}
                                            :available {"TEARS" 1}
                                            :reactions {"TEARS" {:inputs {"GAS" 3} :quantity 1}}})
                      [:required :available]))
      "Use leftover products from other reactions")

  (is (= {:required {}, :available {"TEARS" 16}}
         (select-keys (reduce-requirements {:required {"TEARS" 2}
                                            :available {"TEARS" 18}
                                            :reactions {"TEARS" {:inputs {"GAS" 3} :quantity 1}}})
                      [:required :available]))
      "When there are enough leftovers, no need to start a reaction"))
