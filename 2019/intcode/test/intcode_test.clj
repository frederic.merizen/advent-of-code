(ns intcode-test
  (:require [clojure.string :as string]
            [clojure.test :refer [deftest is testing]]
            [intcode :refer :all]))

(deftest parse-line-test
  (is (= [1 2 4 8] (parse-line "1,2,4,8"))))

(deftest parameter-mode-test
  (is (= [1 0 1 1] (take 4 (parameter-mode 110127))) "should be all digits, in reverse order, but the units and hundreds."))

(deftest position-mode-test
  (is (= 1 (address (position-mode 1) nil)))
  (is (= 10 (read (position-mode 1) {:memory [0 10 7]}))))

(deftest immediate-mode-test
  (is (= 1 (read (immediate-mode 1) nil))))

(deftest relative-mode-test
  (is (= 1 (address (relative-mode -16) {:rel-base 17})))
  (is (= 10 (read (relative-mode -16) {:memory [0 10 7] :rel-base 17}))))

(deftest operation-test
  (letfn [(exec [state {:keys [instr args]}] (instr state args))]
    (testing "addition"
      (is (= {:memory [1 4 5]} (exec {:memory [1 4 nil]} (decode [1 0 1 2] 0)))))
    (testing "multiplication"
      (is (= {:memory [3 4 12]} (exec {:memory [3 4 nil]} (decode [2 0 1 2] 0)))))
    (testing "input"
      (is (= [18] (:memory (exec {:memory [nil] :io (reify IO (in [_] 18))} (decode [3 0] 0))))))
    (testing "output"
      (let [v (volatile! nil)]
        (exec {:memory [2713] :io (reify IO (out [_ val] (vreset! v val)))} (decode [4 0] 0))
        (is (= 2713 @v))))
    (testing "halt"
      (is (= {:done true} (exec {} (decode [99] 0)))))
    (testing "adjust relative base"
      (is (= 2019 (:rel-base (exec {:rel-base 2000} (decode [109 19] 0))))))
    (testing "read relative"
      (is (= {:memory [17 10] :rel-base 2}
             (exec {:memory [nil 10] :rel-base 2} (decode [1201 -1 7 0] 0)))))
    (testing "read beyond known memory"
      (is (= {:memory [5]} (exec {:memory [nil]} (decode [101 5 1000 0] 0)))))
    (testing "write beyond known memory"
      (is (= {:memory [0 0 0 0 0 7]} (exec {:memory []} (decode [1101 3 4 5] 0)))))
    (testing "immediate operand"
      (is (= {:memory [1 4 6]} (exec {:memory [1 4 nil]} (decode [1001 1 2 2] 0))))
      (let [v (volatile! nil)]
        (exec {:io (reify IO (out [_ val] (vreset! v val)))} (decode [104 28] 0))
        (is (= 28 @v))))))

(deftest step-test
  (let [state {:pc 10, :memory "memory"}
        instr {:count 5
               :args "args"
               :instr (fn [s a]
                        (is (= (dissoc state :pc) (dissoc s :pc)) "The instruction should get passed the state")
                        (is (= "args" a) "The instruction sohuld get passed the args")
                        (assoc s :instr-called true))}]
    (with-redefs [decode (fn [m p]
                           (is (= m "memory") "the decoder should get passed the memory from the state")
                           (is (= p 10) "the decoder should get passed the program counter from the state")
                           instr)]
      (let [new-state (step state)]
        (is (:instr-called new-state) "instr should get called")
        (is (= 15 (:pc new-state)) "the program counter should get incremented by the instruction's count")))))
