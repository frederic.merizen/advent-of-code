(ns day-16-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-16 :refer :all]))

(defn has-prefix [prefix infinite-seq]
  (let [actual (vec (take (count prefix) infinite-seq))]
    (= prefix actual)))

(deftest test-has-prefix
  (is (has-prefix [1 1 1] (repeat 1)))
  (is (not (has-prefix [1 1 1] [1 1]))))

(deftest test-pattern
  (is (has-prefix [1 0 -1 0 1 0 -1] (pattern 0)))
  (is (has-prefix [0, 1 1, 0 0, -1 -1, 0 0, 1 1, 0 0, -1 -1] (pattern 1)))
  (is (has-prefix [0 0, 1 1 1, 0 0 0, -1 -1 -1, 0 0 0, 1 1 1, 0 0 0, -1 -1 -1] (pattern 2))))

(deftest test-ones-digit
  (is (= 1 (ones-digit 1)))
  (is (= 1 (ones-digit 21)))
  (is (= 1 (ones-digit -31))))

(deftest test-convolute
  (is (= 62 (convolute [9 8 7 6 5] (apply concat (repeat (range 1 4)))))))

(deftest test-phase
  (is (= [4 8 2 2 6 1 5 8] (phase (range 1 9))))
  (is (= [3 4 0 4 0 4 3 8] (phase (phase (range 1 9))))))

(deftest test-parse
  (is (= [1 2 3] (parse "123"))))

(deftest test-reverse-tail
  (is (= [4 3 2 1 4 3]
         (reverse-tail [1 2 3 4] 5 14))))

(deftest test-offset
  (is (= 303673 (offset [0 3 0 3 6 7 3 2 5 7 7 2 1 2 9 4 4 0 6 3 4 9 1 5 6 5 4 7 4 6 6 4]))))

(deftest test-step
  (is (= [8 5 1 6] (step [8 7 6 5]))))
