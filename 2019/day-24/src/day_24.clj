(ns day-24
  (:require
   [clojure.set :as set]
   [clojure.java.io :as io]
   [clojure.math.numeric-tower :as math]
   [util.collections :refer [first-duplicate]]
   [util.map :as map]))

(defn neighbours [[x y d]]
  #{[(inc x) y d] [(dec x) y d] [x (inc y) d] [x (dec y) d]})

(defn warp-out [[x y d :as cell]]
  (cond
    (= x -1) [1 2 (dec d)]
    (= y -1) [2 1 (dec d)]
    (= x 5) [3 2 (dec d)]
    (= y 5) [2 3 (dec d)]
    :else cell))

(defn warp-in [from to]
  (if-not (= [2 2] (take 2 to))
    [to]
    (let [d (peek to)]
      (apply map vector
             (case from
               [1 2] [(repeat 0) (range 5) (repeat (inc d))]
               [2 1] [(range 5) (repeat 0) (repeat (inc d))]
               [3 2] [(repeat 4) (range 5) (repeat (inc d))]
               [2 3] [(range 5) (repeat 4) (repeat (inc d))])))))

(defn extended-neighbours [[x y d]]
  (into #{}
        (comp
         (map #(warp-out %))
         (mapcat #(warp-in [x y] %)))
        (neighbours [x y d])))

(defn neighbour-count [neighbours coords cells]
  (count (set/intersection cells (neighbours coords))))

(defn next-cell-state [neighbours coords cells]
  (let [c (neighbour-count neighbours coords cells)]
    (when (or (= 1 c)
              (and (= 2 c ) (nil? (cells coords))))
      coords)))

(def grid
  (for [x (range 5)
        y (range 5)]
    [x y 0]))

(defn next-state [cells]
  (set (keep #(next-cell-state neighbours % cells) grid)))

(defn depth-range [cells]
  (let [ds (map peek cells)]
    [(apply min ds)
     (apply max ds)]))

(defn cuboid [min max]
  (for [x (range 5)
        y (range 5)
        :when (not= x y 2)
        d (range min (inc max))]
    [x y d]))

(defn next-extended-state [cells]
  (let [[min max] (depth-range cells)]
    (set (keep #(next-cell-state extended-neighbours % cells) (cuboid (dec min) (inc max))))))

(defn cell-number [[x y]]
  (+ 1 x (* y 5)))

(defn biodiversity-rating [cells]
  (apply + (map #(math/expt 2 (dec (cell-number %))) cells)))

(defn extended-bug-count-after [minutes cells]
  (count (nth (iterate next-extended-state cells)
          minutes)))

(defn render-grid [cells]
  (for [y (range 5)]
    (apply str
           (for [x (range 5)]
             (if (cells [x y 0]) \# \.)))))

(defn render-cuboid [cells]
  (let [by-depth (group-by peek cells)
        [min max] (depth-range cells)]
    (mapcat
     (fn [d]
       (cons
        (str "Depth " d ":\n")
        (render-grid (set (by-depth d)))))
     (range min (inc max)))))

(defn -main
  []
  (let [cells (map/read {:coords #(vector %1 %2 0)})]
    (println (biodiversity-rating (first-duplicate (iterate next-state cells))))
    (println (extended-bug-count-after 200 cells))))
