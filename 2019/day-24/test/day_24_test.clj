(ns day-24-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-24 :refer :all]))

(deftest test-parse
  (is (= #{[2 0 0] [0 1 0]}
         (parse ["..#" "#.."]))))

(deftest test-neighbours
  (is (= #{[1 0 0] [3 0 0] [2 -1 0] [2 1 0]}
         (neighbours [2 0 0]))))

(deftest test-extended-neighbours
  (testing "stay on the same level")
  (is (= #{[0 1 0] [2 1 0] [1 0 0] [1 2 0]}
         (extended-neighbours [1 1 0])))
  (testing "warp out to containing grid"
    (is (= #{[1 0 0] [3 0 0] [2 1 -1] [2 1 0]}
           (extended-neighbours [2 0 0]))))
  (testing "warp in to center grid"
    (is (= #{[1 1 0] [3 1 0] [2 0 0] [0 0 1] [1 0 1] [2 0 1] [3 0 1] [4 0 1]}
           (extended-neighbours [2 1 0])))))

(deftest test-neighbour-count
  (is (zero? (neighbour-count neighbours [1 1 0] (parse ["..." "..." "..."]))))
  (is (= 4 (neighbour-count neighbours [1 1 0] (parse [".#." "#.#" ".#."]))))
  (is (= 4 (neighbour-count neighbours [1 1 0] (parse ["###" "###" "###"])))))

(deftest test-next-cell-state
  (testing "A bug dies (becoming an empty space) unless there is exactly one bug adjacent to it"
    (with-redefs [neighbour-count (constantly 0)]
      (is (nil? (next-cell-state neighbours [1 1 0] #{[1 1 0]}))))
    (with-redefs [neighbour-count (constantly 1)]
      (is (= [1 1 0] (next-cell-state neighbours [1 1 0] #{[1 1 0]}))))
    (with-redefs [neighbour-count (constantly 2)]
      (is (nil? (next-cell-state neighbours [1 1 0] #{[1 1 0]})))))
  (testing "An empty space becomes infested with a bug if exactly one or two bugs are adjacent to it"
    (with-redefs [neighbour-count (constantly 0)]
      (is (nil? (next-cell-state neighbours [1 1 0] #{}))))
    (with-redefs [neighbour-count (constantly 1)]
      (is (= [1 1 0] (next-cell-state neighbours [1 1 0] #{}))))
    (with-redefs [neighbour-count (constantly 2)]
      (is (= [1 1 0] (next-cell-state neighbours [1 1 0] #{}))))
    (with-redefs [neighbour-count (constantly 3)]
      (is (nil? (next-cell-state neighbours [1 1 0] #{}))))))

(deftest test-next-state
  (is (= ["#..#."
          "####."
          "###.#"
          "##.##"
          ".##.."]
         (render-grid (next-state (parse ["....#"
                                          "#..#."
                                          "#..##"
                                          "..#.."
                                          "#...."]))))))

(deftest test-cell-number
  (is (= [16] (map cell-number (parse [ "....."
                                       "....."
                                       "....."
                                       "#...."])))))

(deftest test-biodiversity-rating
  (is (= 2129920 (biodiversity-rating (parse ["....."
                                              "....."
                                              "....."
                                              "#...."
                                              ".#..."])))))

(deftest test-depth-range
  (is (= [-3 5] (depth-range #{[34 55 5] [234 523 2] [258 987 -3]}))))


(println (clojure.string/join "\n"
                              (render-cuboid
                               (->
                                (parse ["....#"
                                        "#..#."
                                        "#..##"
                                        "..#.."
                                        "#...."])
                                next-extended-state))))
