(ns day-23
  (:require
   [clojure.core.async :refer [<!! alts!! chan poll! put! timeout]]
   [intcode :refer [parse-file in out IO run]]))

;;; Logging
(def logger (agent nil))

(defn log [& args] (send-off logger #(apply println %2) args))

;;; Network
(def node-ids (range 50))

(defn set-idle [network address idle] (assoc-in network [:idle address] idle))

(defn node-idle? [network address] (get-in network [:idle address]))

(defn node-ch [network address] (get-in network [:channels address]))

(defn ensure-chan [network address]
  (if (contains? (:channels network) address)
    network
    (-> network
        (assoc-in [:channels address] (chan 2048))
        (set-idle address false))))

(def network (agent (ensure-chan {} 255)))

(defn send-packet' [network packet]
  (let [address (:address packet)
        network (ensure-chan network address)
        ch (node-ch network address)]
    (put! ch packet)
    (set-idle network address false)))

(defn send-packet [packet] (send network send-packet' packet))

(defn blocking-send-packet' [network packet sent]
  (let [network (send-packet' network packet)]
    (deliver sent true)
    network))

(defn blocking-send-packet [packet]
  (let [sent (promise)]
    (send network blocking-send-packet' packet sent)
    @sent))

(defn receive-packet' [network address packet]
  (let [data (some-> (node-ch network address) poll!)]
    (deliver packet data)
    (set-idle network address (nil? data))))

(defn receive-packet [address]
  (let [packet (promise)]
    (send network receive-packet' address packet)
    @packet))

;;; NAT
(defn idle? [network] (every? identity (map #(node-idle? network  %) node-ids)))

(defn run-nat [first-packet]
  (let [ch (node-ch @network 255)
        poll #(alts!! [(timeout 100) ch])]
    (loop [[v] (poll)
           current nil
           previous nil]
      (cond
        v (let [v (assoc v :address 0)]
            (deliver first-packet (:y v))
            (recur (poll) v previous))
        (idle? @network) (if (= current previous)
                           (:y current)
                           (do
                             (blocking-send-packet current)
                             (recur (poll) current current)))
        :else (recur (poll) current previous)))))

;;; Input
(declare read-address)
(declare read-x)
(declare read-y)

(defn read-address [state]
  (assoc state
         :in (:address state)
         :input-fn read-x))

(defn read-x [state]
  (if-let [packet (receive-packet (:address state))]
    (assoc state
           :in (:x packet)
           :input-fn (read-y packet))
    (assoc state :in -1)))

(defn read-y [packet]
  (fn [state]
    (assoc state
           :in (:y packet)
           :input-fn read-x)))

;;; Output
(declare write-address)
(declare write-x)
(declare write-y)

(defn write-address [state val]
  (assoc state
         :out {:address val}
         :output-fn write-x))

(defn write-x [state val]
  (-> state
      (update :out assoc :x val)
      (assoc :output-fn write-y)))

(defn write-y [state val]
  (send-packet (assoc (:out state) :y val))
  (assoc state :output-fn write-address))

(defrecord ComputerState [address input-fn output-fn]
  IO
  (in [this] (input-fn this))
  (out [this val] (output-fn this val)))

(defn computer-state [address] (->ComputerState address read-address write-address))

(defn io [address]
  (let [state (volatile! (computer-state address))]
    (reify IO
      (in [_] (:in (vswap! state in)))
      (out [_ val] (vswap! state out val)))))

(defn future-catch [f]
  (future
    (try (f)
         (catch InterruptedException e)
         (catch Exception e
           (log e)))))

(defn shutdown [vms]
  (doseq [vm vms]
    (future-cancel vm))
  (shutdown-agents))

(defn -main []
  (let [instructions (parse-file "input.txt")
        part-1 (promise)
        part-2 (future-catch #(run-nat part-1))
        vms (doall (for [i node-ids]
                     (future-catch #(run instructions (io i)))))]
    (log "Part 1" @part-1)
    (log "Part 2" @part-2)
    (shutdown vms)))
