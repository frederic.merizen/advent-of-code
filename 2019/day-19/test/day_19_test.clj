(ns day-19-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-19 :refer :all]
   [intcode :refer [in out step]]))

(deftest test-set-mid
  (is (= {:min [10 100], :max [20 500], :axis 1, :mid [15 100]}
         (set-mid {:min [10 100]
                   :max [20 500]
                   :axis 0})))

  (is (= {:min [10 100], :max [20 500], :axis 0, :mid [10 300]}
         (set-mid {:min [10 100]
                   :max [20 500]
                   :axis 1}))))
