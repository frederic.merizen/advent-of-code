(ns day-19
  (:require
   [clojure.string :as string]
   [intcode :refer [parse-file IO run]]))

(defn io [x y]
  (let [state (volatile! {:input [x y]})
        io (reify IO
             (in [_]
               (let [[c] (:input @state)]
                 (vswap! state update :input next)
                 c))
             (out [_ val]
               (vswap! state assoc :output val)))]
    [io state]))

(defn beam-at* [program x y]
  (let [[io state] (io x y)]
    (run program io)
    (= 1 (:output @state))))

(def beam-at? (memoize beam-at*))

(defn traction-count [program]
  (count (filter identity (for [y (range 50), x (range 50)] (beam-at? program x y)))))

(defn square-fits? [{[x0 y0] :pos, :keys [program size]}]
   (and (beam-at? program (+ x0 size -1) y0)
        (beam-at? program x0 (+ y0 size -1))))

(defn flip
  ([f   a b] (f   b a))
  ([f x a b] (f x b a)))

(defn no-flip
  ([f   a b] (f   a b))
  ([f x a b] (f x a b)))

(defn refine-slope [instructions f g estimate gauge bound]
  (loop [estimate estimate
         gauge gauge]
    (if (> gauge bound)
      estimate
      (let [start (int (f gauge estimate))
            estimate (some #(when (g beam-at? instructions % gauge)
                              (g / gauge (dec %)))
                           (map #(+ % start) (range)))]
        (recur estimate (* 10 gauge))))))

(defn left-slope [instructions bound]
  (refine-slope instructions / no-flip ##Inf 10 bound))

(defn top-slope [instructions bound]
  (refine-slope instructions * flip 0 10 bound))

(defn closest-square [instructions size]
  (let [ls (left-slope instructions 1000)
        us (top-slope instructions 1000)
        x (/ (* size (inc us)) (- ls us))
        y (- (* ls x) size)]
    [(double x) (double y)]))

(defn show-sample [program x0 y0 sample-size size]
  (doseq [y (range y0 (+ y0 sample-size))]
    (doseq [x (range x0 (+ x0 sample-size))]
      (print (cond
               (square-fits? {:pos [x y] :program program :size size}) \.
               (beam-at? program x y) \#
               :else \ )))
    (println))
  (println))

(defn -main []
  (let [instructions (parse-file "input.txt")]
    #_(show-sample instructions 750 1030 200 100)
    #_(println (traction-count instructions))
    (println (closest-square instructions 100))))


(comment
  (-> 10000 (* 32) (/ 49) int)
  6530
  (-> 10000 (* 41) (/ 49) int)
  8367)
