(ns day-20-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-20 :refer :all]
   [util.map :as map]))

(deftest test-find-neighbour
  (is (nil?         (find-neighbour {[1 1] \a} [2 2])))
  (is (= [[1 1] \a] (find-neighbour {[1 1] \a} [2 1])))
  (is (= [[1 1] \a] (find-neighbour {[1 1] \a} [0 1])))
  (is (= [[1 1] \a] (find-neighbour {[1 1] \a} [1 0])))
  (is (= [[1 1] \a] (find-neighbour {[1 1] \a} [1 2]))))

(deftest test-merge-labels
  (testing "merge left to right"
    (is (= "ab" (merge-labels [1 1] \a [2 1] \b)))
    (is (= "ab" (merge-labels [2 1] \b [1 1] \a))))
  (testing "merge top to bottom"
    (is (= "ab" (merge-labels [1 1] \a [1 2] \b)))
    (is (= "ab" (merge-labels [1 2] \b [1 1] \a)))))

(deftest test-perimeter
  (is (= #{[0 1] [1 0] [1 2] [2 0] [2 2] [3 1]} (perimeter [[1 1] [2 1]]))))

(deftest test-add-full-label
  (testing "with an ordinary portal"
    (is (= {:portals {"AB" #{[0 2] [0 1]}}}
           (add-full-label {} [0 1] [0 2] "AB"))))
  (testing "when the other matchin portal is found"
    (is (= {:portals {"AB" #{[0 1] [0 2] [5 8] [6 8]}}}
           (add-full-label {:portals {"AB" #{[0 1] [0 2]}}}
                           [5 8] [6 8] "AB"))))
  (testing "with the entry"
    (is (= {:possible-entries #{[0 0] [-1 1] [1 1] [-1 2] [1 2] [0 3]}}
           (add-full-label {} [0 1] [0 2] "AA"))))
  (testing "with the exit"
    (is (= {:possible-exits #{[0 0] [-1 1] [1 1] [-1 2] [1 2] [0 3]}}
           (add-full-label {} [0 1] [0 2] "ZZ")))))

(deftest test-add-half-label
  (testing "without a matching label, store it for when the second half is found"
    (is (= {:labels {[1 1] \a}}
           (add-half-label {} [1 1] \a))))
  (testing "when the second half has already been found, build a portal"
    (is (= {:labels {}
            :portals {"ab" #{[1 1] [1 2]}}}
           (-> {}
               (add-half-label [1 1] \a)
               (add-half-label [1 2] \b))))))

(deftest test-classify-into
  (testing "with a label"
    (is (= {:labels {[1 2] \a}}
           (classify-into {} [[1 2] [:label \a]]))))
  (testing "with something else"
    (is (= {:maze {[1 2] [:wall]}}
           (classify-into {} [[1 2] [:wall]])))))

(deftest test-find-open
  (is (= [3 3]
         (find-open {[1 1] [:wall], [3 1] [:open], [3 3] [:open]}
                    #{[1 1] [2 2] [3 3]}))))

(deftest test-portals-with-destinations
  (is (= [[[1 8] [:portal [9 5] :out]]
          [[9 6] [:portal [2 8] :in]]]
         (portals-with-destinations
          {[2 8] [:open], [9 5] [:open]}
          {:left 2 :right 20 :top 2 :bottom 15}
          [[0 8] [1 8] [9 6] [9 7]]))))

(deftest test-portal-type
  (is (= :in  (portal-type {:left 2 :top 2 :right 15 :bottom 18} [10 8])))
  (is (= :out (portal-type {:left 2 :top 2 :right 15 :bottom 18} [1 5])))
  (is (= :out (portal-type {:left 2 :top 2 :right 15 :bottom 18} [5 1])))
  (is (= :out (portal-type {:left 2 :top 2 :right 15 :bottom 18} [16 7])))
  (is (= :out (portal-type {:left 2 :top 2 :right 15 :bottom 18} [9 19]))))

(def maze
  ["         A           "
   "         A           "
   "  #######.#########  "
   "  #######.........#  "
   "  #######.#######.#  "
   "  #######.#######.#  "
   "  #######.#######.#  "
   "  #####  B    ###.#  "
   "BC...##  C    ###.#  "
   "  ##.##       ###.#  "
   "  ##...DE  F  ###.#  "
   "  #####    G  ###.#  "
   "  #########.#####.#  "
   "DE..#######...###.#  "
   "  #.#########.###.#  "
   "FG..#########.....#  "
   "  ###########.#####  "
   "             Z       "
   "             Z       "])

(deftest test-bounds
  (is (= {:left 2, :top 2, :right 18, :bottom 16} (bounds (:maze (with-redefs [post-process identity] (map/parse maze parse-options)))))))

(def parsed (map/parse maze parse-options))

(deftest test-parse
  (is (= [:wall] (get-in parsed [:maze [2 2]])))
  (is (= [:open] (get-in parsed [:maze [9 2]])))
  (is (= [:portal [9 6] :out] (get-in parsed [:maze [1 8]])))
  (is (= [:portal [2 8] :in] (get-in parsed [:maze [9 7]])))
  (is (= [9 2] (:entry parsed)))
  (is (= [13 16] (:exit parsed))))

(deftest test-shortest-path
  (is (= 23 (shortest-path parsed))))
