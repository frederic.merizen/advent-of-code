(ns day-20
  (:require
   [clojure.core.match :refer [match]]
   [clojure.core.match.regex]
   [util.explore :as explore]
   [util.map :as map]))

;;; Parse the goddamned thing
(defn parse-char [c]
  (match (str c)
         "#" [:wall]
         "." [:open]
         #"[A-Z]" [:label c]
         _  nil))

(defn find-neighbour [coords-to-objects coords]
  (first (select-keys coords-to-objects (map/neighbours-4 coords))))

(defn merge-labels [coords-a label-a coords-b label-b]
  (apply str (vals (sorted-map coords-a label-a coords-b label-b))))

(defn perimeter [coords]
  (apply disj (into #{} (mapcat map/neighbours-4) coords) coords))

(defn add-full-label [m coords-a coords-b label]
  (match label
         "AA" (assoc m :possible-entries (perimeter [coords-a coords-b]))
         "ZZ" (assoc m :possible-exits (perimeter [coords-a coords-b]))
         _    (update-in m [:portals label] (fnil conj #{}) coords-a coords-b)))

(defn add-half-label [m coords label]
  (if-let [[n-coords n-label] (find-neighbour (:labels m) coords)]
    (let [full-label (merge-labels coords label n-coords n-label)]
      (-> m
          (update :labels dissoc n-coords)
          (add-full-label coords n-coords full-label)))
    (assoc-in m [:labels coords] label)))

(defn classify-into [m [coords c]]
  (match c
         [:label l] (add-half-label m coords l)
         _          (assoc-in m [:maze coords] c)))

(defn find-open [maze candidates]
  (first (keep (fn [[coords object]]
                 (when (and (candidates coords)
                            (= [:open] object))
                   coords))
               maze)))

(defn portal-type [{:keys [left top right bottom]} [x y]]
  (if (or (#{(dec left) (inc right)} x) (#{(dec top) (inc bottom)} y))
    :out
    :in))

(defn portals-with-destinations [maze bounds coordinates]
  (let [[[from-a to-b] [from-b to-a]]
        (keep #(when-let [to (find-open maze (set (map/neighbours-4 %)))]
                 [% to])
              coordinates)]
    [[from-a [:portal to-a (portal-type bounds from-a)]]
     [from-b [:portal to-b (portal-type bounds from-b)]]]))

(defn bounds [maze]
  (let [x (map first (keys maze))
        y (map second (keys maze))]
    {:left (apply min x)
     :right (apply max x)
     :top (apply min y)
     :bottom (apply max y)}))

(defn add-portals [maze portals]
  (let [bounds (bounds maze)]
    (into maze
          (mapcat #(portals-with-destinations maze bounds %))
          (vals portals))))

(defn post-process [{:keys [maze possible-entries possible-exits portals]}]
    {:maze (add-portals maze portals)
     :entry (find-open maze possible-entries)
     :exit (find-open maze possible-exits)})

(defn classify [l]
  (post-process (reduce classify-into {} l)))

(def parse-options
  {:char parse-char
   :collect classify})

;; Find our way around it
(defn destinations [maze pos]
  (keep #(match (maze %)
                [:open] {:pos %}
                [:portal dest _] {:pos dest}
                _ nil)
        (map/neighbours-4 pos)))

(defn shortest-path [{:keys [maze entry exit]}]
  (:cost
   (explore/a* entry
               #(= exit %)
               #(destinations maze %))))

(defn nested-destinations [maze [x0 y0 z0]]
  (keep (fn [[x y :as pos]]
          (match (maze pos)
                 [:open]                                {:pos [x y z0]}
                 [:portal [x1 y1] :in]                  {:pos [x1 y1 (inc z0)]}
                 [:portal [x1 y1] :out] (when (pos? z0) {:pos [x1 y1 (dec z0)]})
                 _                                      nil))
        (map/neighbours-4 [x0 y0])))

(defn shortest-nested-path [{:keys [maze entry exit]}]
  (let [entry (conj entry 0)
        exit (conj exit 0)]
    (:cost
     (explore/a* entry
                 #(= exit %)
                 #(nested-destinations maze %)))))

(defn -main []
  (let [maze (map/read parse-options)]
    (println (shortest-path maze))
    (println (shortest-nested-path maze))))
