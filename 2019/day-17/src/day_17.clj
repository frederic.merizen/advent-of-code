(ns day-17
  (:require
   [clojure.string :as string]
   [intcode :refer [parse-file IO run]]
   [lanterna.terminal :as t]))

(defn io [term & input-lines]
  (let [input (volatile! (map int (str (string/join "\n" input-lines) "\n")))
        screen (volatile! {:blank false, :y 0})]
    (reify IO
      (in [_]
        (let [[c] @input]
          (vswap! input next)
          c))
      (out [_ val]
        (cond
          (= val 10) (let [{:keys [blank y]} @screen
                           y (if blank 0 (inc y))]
                       (vreset! screen {:blank true, :y y})
                       (t/move-cursor term 0 y))
          (< val 256) (do
                        (vswap! screen assoc :blank false)
                        (t/put-character term (char val)))
          :else (t/put-string term (str val)))))))

(defn alignment [l-cs]
  (apply + (mapcat (fn [[l cs]] (map #(* (dec l) %) cs)) l-cs)))

(def part-1
  (alignment [[9 [26]] [11 [32 34]] [15 [14]] [17 [2 8 32]] [21 [8]] [29 [10]] [35 [12 16]] [41 [12]]]))

(defn -main []
  (let [instructions (parse-file "input.txt")
        term (t/get-terminal :text)]
    (t/in-terminal term
      (t/move-cursor term 0 0)
      (run (assoc instructions 0 2) (io term
                                        "A,B,A,C,A,B,C,A,B,C"
                                        "R,8,R,10,R,10"
                                        "R,4,R,8,R,10,R,12"
                                        "R,12,R,4,L,12,L,12"
                                        "y"))
      (t/get-key-blocking term))))
