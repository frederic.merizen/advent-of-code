(ns day-08-test
  (:require [clojure.string :as string]
            [clojure.test :refer [deftest is testing]]
            [day-08 :refer :all]))

(deftest test-parse-pixels
  (is (= [1 2 3 4 5 6 7 8 9 0 1 2]
         (parse-pixels "123456789012"))))

(deftest test-split-into-layers
  (is (= [[1 2 3
           4 5 6]
          [7 8 9
           0 1 2]]
         (split-into-layers [1 2 3 4 5 6 7 8 9 0 1 2] 3 2))))

(deftest test-count-digit
  (is (= 4 (count-digit [1 2 0
                         0 0 0]
                        0))))

(deftest test-find-fewest-zeros
  (is (= [1 2 1
          1 1 2]
         (find-fewest-zeros [[1 2 1
                              1 1 2]
                             [1 2 0
                              0 0 0]]))))

(deftest test-part-1
  (is (= 8
         (part-1 [1 2 1
                  1 1 2
                  1 2 0
                  0 0 0]
                 3 2))))

(deftest test-nilify-transparent
  (is (= [1 0 nil
          nil 1 1]
         (nilify-transparent [1 0 2
                              2 1 1]))))

(deftest test-stack-layers
  (is (= [1 0 1
          0 1 1]
         (stack-layers [[1   nil nil
                         0   nil nil]
                        [1   0   1
                         nil nil 1]
                        [nil 1   nil
                         nil 1   nil]
                        [nil nil 1
                         1   0   0]]))))

(deftest test-format-layer
  (is (= (str " █ \n"
              "█  \n")
         (format-layer [0 1 0
                        1 0 0]
                       3))))
