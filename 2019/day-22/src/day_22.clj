(ns day-22
  (:require
   [clojure.core.match :refer [match]]
   [clojure.java.io :as io]
   [instaparse.core :as insta]
   [util.numeric :refer [xgcd]]))

;;; Parser

(def parse-shuffles
  (insta/parser
   "<shuffles> = (shuffle <newline>)*
    <shuffle> = deal-new | cut | deal-increment
    deal-new = <'deal into new stack'>
    cut =  <'cut '> count
    deal-increment = <'deal with increment '> count
    <count> = #'-?[0-9]+'
    newline = '\n'
    "))

(defn fixup-shuffles [shuffles]
  (map (fn [[instr count :as shuffle]]
         (if count
           [instr (Integer/parseInt count)]
           shuffle))
       shuffles))

(defn parse-input [i]
  (fixup-shuffles (parse-shuffles i)))

;;; Simplify a stack of shuffles to a normal form

(defn reduce-deal-new [shuffles count]
  (mapcat #(match %
                  [:deal-new] [[:deal-increment (mod -1 count)] [:cut 1]]
                  _           [%])
          shuffles))

(defn simplify-pairs
  ([shuffles count]
   (simplify-pairs [] shuffles count))
  ([acc shuffles count]
   (match shuffles
          ([] :seq)
          acc

          ([s] :seq)
          (conj acc s)

          ([s1 s2 & tail] :seq)
          (match [s1 s2]
                 [[:deal-increment x] [:deal-increment y]]
                 (recur acc (conj tail [:deal-increment (mod (*' x y) count)]) count)

                 [[:cut x] [:cut y]]
                 (recur acc (conj tail [:cut (mod (+' x y) count)]) count)

                 [[:cut x] [:deal-increment y]]
                 (let [tail (conj tail
                                  [:cut (mod (*' x y) count)] ; conj adds to the beginning of `tail`,
                                  [:deal-increment y])]       ; reversing the order of added elements
                   (if (seq acc)
                     (recur (pop acc) (conj tail (peek acc)) count)
                     (recur acc tail count)))

                 [_ _]
                 (recur (conj acc s1) (rest shuffles) count)))))

(defn simplify-units [shuffles]
  (keep (fn [s]
          (match s
             [:deal-increment 1] nil
             [:cut 0] nil
             _ s))
        shuffles))

(defn simplify [shuffles count]
  (simplify-units (simplify-pairs (reduce-deal-new shuffles count) count)))

;;; Forward shuffle (position in initial deck to position in shuffled deck)

(def shuffle-by-name
  {:deal-new (fn [pos count]
               (- count pos 1))
   :cut (fn [n pos count]
          (let [n (mod n count)]
            (mod (- pos n) count)))
   :deal-increment (fn [n pos count]
                     (mod (* n pos) count))})

(defn shuffle [[shuffle & args] pos count]
  ((apply partial (shuffle-by-name shuffle) args) pos count))

(defn apply-shuffles [shuffles pos count]
  (reduce #(shuffle %2 %1 count) pos shuffles))

(defn card-position [shuffles pos count]
  (apply-shuffles (simplify shuffles count) pos count))

;;; Reverse shuffle (position in shuffled deck to position in initial deck)

(def rev-by-name
  {:deal-new (fn [pos count]
               (- count pos 1))
   :cut (fn [n pos count]
          (let [n (mod n count)]
            (mod (+ pos n) count)))
   :deal-increment (fn [n pos count]
                     (let [[_ inv _] (xgcd n count)]
                       (mod (* inv pos) count)))})

(defn rev [[shuffle & args] pos count]
  ((apply partial (rev-by-name shuffle) args) pos count))

(defn rev-shuffles [shuffles pos count]
  (reduce #(rev %2 %1 count) pos (reverse shuffles)))

(defn pow [shuffles n count]
  (let [q (quot n 1000)
        r (rem n 1000)]
    (simplify
     (apply concat
            (when (pos? q)
              (apply concat (repeat 1000 (pow shuffles q count))))
            (repeat r shuffles))
     count)))

(defn rev-card-position [shuffles exponent pos count]
  (rev-shuffles (pow (simplify shuffles count) exponent count) pos count))

(defn -main []
  (let [shuffles (parse-input (slurp "input.txt"))]
    (println (card-position shuffles 2019 10007))
    (println (biginteger (rev-card-position shuffles 101741582076661 2020 119315717514047)))))
