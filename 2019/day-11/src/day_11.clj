(ns day-11
  (:require
    [intcode :refer [parse-file IO run]]))

(declare turn)

(defn paint [{:keys [position] :as state} colour]
  (-> state
      (update :paint assoc position colour)
      (assoc :mode turn)))

(defn rotate [[dx dy] direction]
  (let [sgn (dec (* direction 2))]
    [(* dy sgn -1) (* dx sgn)]))

(defn forward [{:keys [position direction] :as state}]
  (assoc state :position (map + position direction)))

(defn turn [state direction]
    (-> state
        (update :direction rotate direction)
        forward
        (assoc :mode paint)))

(defn camera [{:keys [paint position]}]
  (get paint position 0))

(defn initial-robot-state [initial-paint]
  {:direction [0 -1] :position [0 0] :paint initial-paint :mode paint})

(defn robot [initial-paint]
  (let [state (volatile! (initial-robot-state initial-paint))
        io (reify IO
             (in [_]
               (camera @state))
             (out [_ val]
               (vswap! state #((:mode %) % val))))]
    [state io]))

(defn count-painted [instructions]
  (let [[state io] (robot {})]
    (run instructions io)
    (count (:paint @state))))

(defn bounding-box [paint]
  (let [coordinates (keys paint)
        xs (map first coordinates)
        ys (map fnext coordinates)]
    [[(apply min xs) (apply min ys)] [(apply max xs) (apply max ys)]]))

(defn render [paint]
  (let [[[xmin ymin] [xmax ymax]] (bounding-box paint)]
    (for [y (range ymin (inc ymax))]
      (apply str
             (for [x (range xmin (inc xmax))]
               (if (= 1 (paint [x y]))
                 \█ \ ))))))

(defn paint-registration [instructions]
  (let [[state io] (robot {[0 0] 1})]
    (run instructions io)
    (apply str (map #(str % \newline) (render (:paint @state))))))

(defn -main
  []
  (let [instructions (parse-file "input.txt")]
    (println "part 1")
    (println (count-painted instructions))
    (println "part 2")
    (print (paint-registration instructions))))
