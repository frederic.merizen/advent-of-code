(ns day-10
  (:require
    [util.map :as map]))

(defn slope [[x1 y1] [x2 y2]]
  (let [dx (- x2 x1)
        dy (- y2 y1)]
    (when-not (= [x1 y1] [x2 y2])
      [(if (neg? dx) 2 1)
       (if (= (neg? dx) (neg? dy)) 2 1)
       (if (zero? dx)
         (* ##Inf dy)
         (/ dy dx))])))

(defn asteroids-by-slope [asteroid asteroids]
  (dissoc (group-by #(slope asteroid %) asteroids) nil))

(defn max-visible-asteroids [asteroids]
  (apply max (for [asteroid asteroids]
               (count (asteroids-by-slope asteroid asteroids)))))

(defn best-asteroid [asteroids]
  (apply max-key #(count (asteroids-by-slope % asteroids)) asteroids))

(defn closest [asteroid asteroids]
  (apply min-key #(map/dist asteroid %) asteroids))

(defn part-2 [asteroids]
  (let [asteroid (best-asteroid asteroids)
        by-slope (asteroids-by-slope asteroid asteroids)
        slopes (vec (sort (keys by-slope)))
        targets (by-slope (nth slopes 199))
        [x y](closest asteroid targets)]
    (+ (* x 100) y)))

(comment
  (asteroid-positions [".##" "#.#"])
  (slope [5 4] [3 2])
  (slope [5 5] [5 2])
  (slope [4 4] [3 4])
  (asteroids-by-slope [1 1] [[1 1] [2 2] [3 3]])
  (let [as (asteroid-positions
            ["......#.#."
             "#..#.#...."
             "..#######."
             ".#.#.###.."
             ".#..#....."
             "..#....#.#"
             "#..#....#."
             ".##.#..###"
             "##...#..#."
             ".#....####"])]
    (asteroids-by-slope [5 8] as)
    (best-asteroid as))
  (map/dist [1 1] [1 1])
  (map/dist [1 1] [2 1])
  (closest [1 2] [[3 4] [5 6] [3 3]]))

(defn -main
  []
  (let [asteroids (map/read)]
    (println (max-visible-asteroids asteroids))
    (println (part-2 asteroids))))
