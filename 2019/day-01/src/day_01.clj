(ns day-01
  (:require
   [clojure.java.io :as io]))

(defn module-fuel [mass]
  (-> mass (quot 3) (- 2)))

(defn sum [args]
  (apply + args))

(defn iterated-module-fuel [mass]
  (sum (take-while pos? (rest (iterate module-fuel mass)))))

(defn read-input [reader]
  (map #(Integer/parseInt %) (line-seq reader)))

(defn -main
  []
  (with-open [input (io/reader "input.txt")]
    (let [masses (read-input input)]
      (println (sum (map module-fuel masses)))
      (println (sum (map iterated-module-fuel masses))))))
