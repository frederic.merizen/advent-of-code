(ns day-12-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-12 :refer :all]))

(deftest test-parse-moon
  (is (= {:pos [-4 -14 8]
          :vel [0 0 0]}
         (parse-moon "<x=-4, y=-14, z=8>"))))

(deftest test-gravity
  (is (= [-1 0 1]
         (gravity {:pos [1 2 3]} {:pos [-1 2 7]}))))

(deftest test-add-vec
  (is (= [12 15 18]
         (add-vec [1 2 3] [4 5 6] [7 8 9]))))

(deftest test-apply-gravity'
  (is (= {:pos [0 0] :vel [1 1]}
         (apply-gravity' {:pos [0 0] :vel[0 0]} [{:pos [0 0] :vel[0 0]} {:pos [1 1] :vel [1 1]}]))))

(deftest test-apply-gravity
  (is (= [{:pos [0 0], :vel [1 1]} {:pos [1 1], :vel [0 0]}]
         (apply-gravity [{:pos [0 0] :vel [0 0]} {:pos [1 1] :vel [1 1]}]))))

(deftest test-apply-velocity'
  (is (= {:pos [2 5], :vel [1 3]}
         (apply-velocity' {:pos [1 2] :vel [1 3]}))))

(deftest test-apply-velocity
  (is (= [{:pos [0 0], :vel [0 0]} {:pos [2 2], :vel [1 1]}]
         (apply-velocity [{:pos [0 0] :vel [0 0]} {:pos [1 1] :vel [1 1]}]))))

(deftest test-energy
  (is (= 6 (energy :pos {:pos [1 2 3]}))))

(deftest test-total-energy
  (is (= 90 (total-energy {:pos [1 2 3] :vel [4 5 6]}))))

(deftest test-axis-period
  (let [ms[{:pos [0 0] :vel [0 0]}
           {:pos [1 2] :vel [0 0]}]]
    (is (= [0 4] (axis-period ms 0)))
    (is (= [0 6] (axis-period ms 1)))))
