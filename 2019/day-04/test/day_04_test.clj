(ns day-04-test
  (:require
   [day-04 :refer :all]
   [clojure.test :refer [deftest is testing]]))

(deftest test-number
  (is (= 654321 (number [6 5 4 3 2 1]))))

(deftest test-password-set
  (is (= #{123 456} (password-set 100 1000 [[1 2 3] [4 5 6] [7 6 5 4]]))
      "should filter out 7654 because it is above the upper limit")
  (is (= #{123 456} (password-set 100 1000 [[4 5] [1 2 3] [4 5 6]]))
      "should filter out 45 because it is below the lower limit"))

(deftest test-part-1
  (let [ps (part-1-passwords 1000 300000)]
    (is (not (ps 0)) "should be excluded by lower bound")
    (is (not (ps 555555)) "should be excluded by upper bound")
    (is (ps 111111) "meets all criteria")
    (is (not (ps 223450)) "should be rejected because of decreasing pair 50")
    (is (not (ps 123789)) "should be rejected because it has no double")))

(deftest test-part-2
  (let [ps (part-2-passwords 1000 300000)]
    (is (ps 112233) "meets all criteria")
    (is (not (ps 123444)) "should be rejected because the repeated 44 is part of a larger group of 444")
    (is (ps 111122) "meets the criteria (even though 1 is repeated more than twice, it still contains a double 22)")))
