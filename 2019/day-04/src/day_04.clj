(ns day-04
  (:require
   [clojure.core.logic :refer :all]
   [clojure.core.logic.fd :as fd]))

(defn number [digits]
  (reduce #(+ (* %1 10) %2) 0 digits))

(defn password-set [lower upper digits-list]
  (into #{}
        (keep (fn [digits]
                (let [n (number digits)]
                  (when (<= lower n upper)
                    n))))
        digits-list))

(defne increasing-digits [a b c d e f]
  ([_ _ _ _ _ _]
   (fd/in a b c d e f (fd/interval 9))
   (fd/eq
    (<= a b)
    (<= b c)
    (<= c d)
    (<= d e)
    (<= e f))))

(defn part-1-passwords [lower upper]
  (password-set lower upper
                (run* [q]
                  (fresh [a b c d e f]
                    (increasing-digits a b c d e f)
                    (== q [a b c d e f])
                    (conde
                     [(== a b)]
                     [(== b c)]
                     [(== c d)]
                     [(== d e)]
                     [(== e f)])))))

(defn part-2-passwords [lower upper]
  (password-set lower upper
                (run* [q]
                  (fresh [a b c d e f]
                    (increasing-digits a b c d e f)
                    (== q [a b c d e f])
                    (conde
                     [         (== a b) (!= b c)]
                     [(!= a b) (== b c) (!= c d)]
                     [(!= b c) (== c d) (!= d e)]
                     [(!= c d) (== d e) (!= e f)]
                     [(!= d e) (== e f)])))))

(defn -main
  []
  (println (count (part-1-passwords 124075 580769)))
  (println (count (part-2-passwords 124075 580769))))
