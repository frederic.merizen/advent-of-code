;; # Day 2

(ns day-02
  (:require
   [better-cond.core :as b :refer [defnc]]
   [clojure.java.io :as io]
   [nextjournal.clerk :as clerk]))

(def input
  (with-open [rdr (io/reader (io/resource "day02.txt"))]
    (vec (line-seq rdr))))

(def moves
  [{:id :stone,   :score 1, :icon '🪨}
   {:id :paper,   :score 2, :icon '📃}
   {:id :scissors, :score 3, :icon '✂️}])

(def score->move
  (into {} (map #(vector (:score %) %) moves)))

(def decode-them
  (into {} (map vector "ABC" moves)))

(def decode-me
  (into {} (map vector "XYZ" moves)))

;; ## Part 1

(def guide
  (map (fn [[them _ me]] {:them (decode-them them) :me (decode-me me)}) input))

(defn defeater [x]
  (-> x :score (mod 3) inc score->move))

(map defeater moves)

(defnc outcome-score [{:keys [me them]}]
  (= (defeater me) them) 0
  (= me them) 3
  6)

(defn shape-score [round]
  (-> round :me :score))

(defn round-score [round]
  (+ (outcome-score round) (shape-score round)))

(defn total-score [moves]
  (apply + (map round-score moves)))

(defn game-table [moves]
  (map (fn [{{them :icon} :them, {me :icon} :me :as round}]
         {:them them, :me me
          :outcome (outcome-score round), :shape (shape-score round) :score (round-score round)})
       guide))

(clerk/table (game-table guide))

(total-score guide)

;; # Part 2

(defn defeatee [x]
  (-> x :score (- 2) (mod 3) inc score->move))

(map defeatee moves)

(defnc decode-step [[them _ me]]
  :let [them (decode-them them)
        response (case me
                   \X defeatee
                   \Y identity
                   \Z defeater)]
  {:me (response them), :them them})

(def actual-guide (map decode-step input))

(clerk/table (game-table actual-guide))

(total-score actual-guide)
