(ns day-01
  (:require
    [clojure.java.io :as io]))

(def input
  (with-open [rdr (io/reader (io/resource "day01.txt"))]
    (vec (line-seq rdr))))

(def food-by-elf
  (->> input
       (partition-by #{""})
       (filter #(not= [""] %))
       (map #(apply + (map read-string %)))))

(def max-food (apply max food-by-elf))

(def max-3-food
  (->> food-by-elf
       (sort-by -)
       (take 3)
       (apply +)))
