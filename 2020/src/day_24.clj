(ns day-24
  (:require
   [instaparse.core :as insta]
   [common]))

(def tiles-parser
  (insta/parser
   "<tiles> = tile *
    tile = direction * <'\n'>
    <direction> = 'e' | 'se' | 'sw' | 'w' | 'nw' | 'ne'"))

(defn parse [s]
  (->> s
       tiles-parser
       (insta/transform
        {:tile vector})))

(def input
  (->> (common/input)
       slurp
       parse))

(def directions
  {"e"  #(-> % (update :e inc))
   "se" #(-> % (update :e inc) (update :ne dec))
   "sw" #(-> %                 (update :ne dec))
   "w"  #(-> % (update :e dec))
   "nw" #(-> % (update :e dec) (update :ne inc))
   "ne" #(-> %                 (update :ne inc))})

(defn position [instructions]
  (reduce #((directions %2) %1)
          {:e 0 :ne 0}
          instructions))

(defn flip [tiles tile]
  (if (tiles tile)
    (disj tiles tile)
    (conj tiles tile)))

(def black-tiles
  (reduce flip #{} (map position input)))

(def part-1 (count black-tiles))

(def neighbours
  (apply juxt (vals directions)))

(defn black-neighbour-counts [black]
  (reduce #(update %1 %2 (fnil inc 0))
          {}
          (mapcat neighbours black)))

(defn next-day [black]
  (let [neigh (black-neighbour-counts black)]
    (concat (keep (fn [[pos count]] (when (= 2 count) pos))
                  neigh)
            (filter (fn [pos] (= 1 (neigh pos)))
                  black))))

(def tiles-on-day (iterate next-day black-tiles))

(def part-2 (->> tiles-on-day (drop 100) first count))

(defn -main []
  (println part-1)
  (println part-2))
