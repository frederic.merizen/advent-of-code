(ns day-17
  (:require
   [clojure.set :as set]
   [clojure.math.combinatorics :as combo]
   [common]))

(def init
  (apply concat
         (map-indexed (fn [y row]
                        (keep-indexed
                         (fn [x cell]
                           (when (= \# cell)
                             [x y]))
                         row))
                      (common/lines (common/input)))))

(def neighbour-offsets
  (memoize (fn [dim]
             (filter #(not (every? zero? %))
                     (apply combo/cartesian-product (repeat dim (range -1 2)))))))

(defn neighbours [pos]
  (map #(map + %1 %2) (neighbour-offsets (count pos)) (repeat pos)))

(defn active-neighbour-counts [active]
  (reduce #(update %1 %2 (fnil inc 0))
          {}
          (mapcat neighbours active)))

(defn coords-per-neighbour-count [active]
  (into {}
        (map (fn [[k v]] [k (map first v)]))
        (group-by second (active-neighbour-counts active))))

(defn next-gen [active]
  (let [{two-neighbours 2, three-neighbours 3} (coords-per-neighbour-count active)]
    (into (set three-neighbours) (filter active two-neighbours))))

(defn puffed-up [dim coords]
  (let [filler (repeat (- dim 2) 0)]
    (into #{}
          (map #(into % filler))
          coords)))

(defn count-after-boot [dim]
  (->> init
       (puffed-up dim)
       (iterate next-gen)
       (drop 6)
       first
       count))

(defn -main []
  (println (time (count-after-boot 3)))
  (println (time (count-after-boot 4))))
