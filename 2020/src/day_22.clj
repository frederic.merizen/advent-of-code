(ns day-22
  (:require
   [common]
   [instaparse.core :as insta]))

(def input-parser
  (insta/parser
   "<decklist> = deck <blank> deck
    blank = '\n'
    deck = <player> cards
    player = <'Player '> #'[0-9]+' <':\n'>
    <cards> = ( #'[0-9]+' <'\n'> ) +"))


(defn parse-input [s]
  (->> s
       input-parser
       (insta/transform
        {:deck #(->> %& (map read-string) (map char) (apply str))})))

(def input
  (-> (common/input) slurp parse-input))

(defn deal-cards [decks]
  [(mapv #(-> % first int) decks)
   (mapv #(subs % 1) decks)])

(defn combat-round-winner [played]
  (apply max-key played (range (count played))))

(defn cut [n cards]
  (str (char (cards n)) (char (cards (- 1 n)))))

(defn take-cards [decks round-winner cards]
  (update decks round-winner str cards))

(defn play-combat-round [decks]
  (let [[played decks] (deal-cards decks)
        w (combat-round-winner played)]
    (take-cards decks w (cut w played))))

(defn winning-deck [[d1 d2]]
  (cond
    (empty? d1) d2
    (empty? d2) d1))

(defn combat-game [decks]
  (->> decks
       (iterate play-combat-round)
       (some winning-deck)))

(defn score [cards]
  (apply +
         (map
          (fn [rank card] (* (int card) rank))
          (range (count cards) 0 -1)
          cards)))

(def part-1 (-> input combat-game score))

(declare recursive-combat-game-winner)

(defn subdecks [[p1 p2] [d1 d2]]
  (when (and (<= p1 (count d1))
             (<= p2 (count d2)))
    [(subs d1 0 p1) (subs d2 0 p2)]))

(defn recursive-combat-round-winner [played decks]
  (if-let [decks (subdecks played decks)]
    (recursive-combat-game-winner decks)
    (combat-round-winner played)))

(defn game-winner [[d1 d2]]
  (cond
    (empty? d1) 1
    (empty? d2) 0))

(defn recursive-combat-game [decks]
  (loop [history #{}
         decks   decks]
    (if (history decks)
      {:winner 0}
      (let [history (conj history decks)
            [played decks] (deal-cards decks)
            w (recursive-combat-round-winner played decks)
            decks (take-cards decks w (cut w played))
            gw (game-winner decks)]
        (if gw
          {:decks decks :winner gw}
          (recur history decks))))))

(defn recursive-combat-game-winner [decks]
  (:winner (recursive-combat-game decks)))

(defn recursive-combat-score [decks]
  (->> decks recursive-combat-game :decks (some seq) score))

(defn part-2 []
  (recursive-combat-score input))

(defn -main []
  (println part-1)
  (println (time (part-2))))
