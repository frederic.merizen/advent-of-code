(ns day-25)

(defn rounds [subject-number]
  (iterate #(-> % (* subject-number) (mod 20201227)) 1))

(defn loop-size [public-key]
  (first (keep-indexed #(when (= %2 public-key) %1) (rounds 7))))

(def card-public-key 18356117)
(def door-public-key 5909654)
(def card-loop-size (loop-size card-public-key))
(def door-loop-size (loop-size door-public-key))

(defn transform [subject-number loop-size]
  (first (drop loop-size (rounds subject-number))))

(def encryption-key (transform card-public-key door-loop-size))

(defn -main []
  (println encryption-key))
