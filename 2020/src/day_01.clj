(ns day-01
  (:require
   [common]
   [multiset.core :as ms]))

(defn keep-first [seq pred]
  (->> seq (keep pred) first))

(defn find-pair
  ([expenses]
   (find-pair expenses 2020))

  ([expenses target]
   (keep-first expenses
               #(when-let [n ((disj expenses %) (- target %))]
                  [% n]))))

(defn find-triplet [expenses]
  (keep-first expenses
              #(let [target (- 2020 %)]
                 (when-let [pair (find-pair (disj expenses %) target)]
                   (conj pair %)))))

(defn -main
  []
  (let [expenses (->> (common/input) common/lines (map read-string) (apply ms/multiset))]
    (println (time (apply * (find-pair expenses))))
    (println (time (apply * (find-triplet expenses))))))
