(ns day-07
  (:require
   [common]
   [instaparse.core :as insta]
   [clojure.test :refer [with-test is]]))

(def bag-parser
  (insta/parser
   "<bags>    = (bag <'\n'>)*
    bag       = container <' bags contain '> contents <'.'>
    container = colour
    contents  = (bag-spec (<', '> bag-spec)*) | <'no other bags'>
    bag-spec  = count <' '> colour <#' bags?'>
    count     = #'[0-9]'
    colour    = #'[a-z]+ [a-z]+'"))

(defn parse-bags [s]
  (->> s
       bag-parser
       (insta/transform
        {:count     (fn [c] [:count (Integer/parseInt c)])
         :bag-spec  #(into {} %&)
         :contents  (fn [& cs] [:contents (vec cs)])
         :container (fn [[_ c]] [:container c])
         :bag       #(into {} %&)})))

(with-test
  (defn container-by-contents [{:keys [container contents]}]
    (into {} (map (fn [{:keys [colour]}]
                    [colour #{container}])
                  contents)))

  (is (= {"red" #{"blue"}, "white" #{"blue"}}
         (container-by-contents {:container "blue" :contents [{:colour "red"}
                                                              {:colour "white"}]}))))

(with-test
  (defn containers-by-contents [bags]
    (reduce #(merge-with into %1 (container-by-contents %2)) {} bags))

  (is (= {"red"   #{"blue", "yellow"}
          "white" #{"blue"}}
         (containers-by-contents [{:container "yellow" :contents [{:colour "red"}]}
                                  {:container "blue"   :contents [{:colour "red"}
                                                                  {:colour "white"}]}]))))

(defn add-next-targets-contents [by-contents {:keys [targets found]}]
  (let [target (peek targets)
        new-targets (by-contents target)]
    {:targets (into (pop targets) new-targets)
     :found (into found new-targets)}))

(with-test
  (defn transitive-containers [target bags]
    (let [by-contents (containers-by-contents bags)]
      (->> {:targets [target], :found #{}}
           (iterate #(add-next-targets-contents by-contents %))
           (filter #(empty? (:targets %)))
           first
           :found)))

  (is (= #{"yellow" "blue"}
         (transitive-containers "red"
                                [{:container "yellow" :contents [{:colour "red"}]}
                                 {:container "blue"   :contents [{:colour "yellow"}
                                                                 {:colour "white"}]}]))))

(with-test
  (defn contents-by-container [bags]
    (into {} (map (juxt :container :contents)) bags))

  (is (= {"yellow" [{:colour "red"}]
          "blue"   [{:colour "yellow"}
                    {:colour "white"}]}
         (contents-by-container [{:container "yellow" :contents [{:colour "red"}]}
                                 {:container "blue"   :contents [{:colour "yellow"}
                                                                 {:colour "white"}]}]))))

(defn bags-inside [target by-container]
  (let [direct-contents (by-container target)
        transitive-count (apply + (map (fn [{:keys [count colour]}] (* count (bags-inside colour by-container)))
                                     direct-contents))]
    (apply + transitive-count (->> direct-contents (map :count)))))

(defn -main []
  (let [bags (-> (common/input) slurp parse-bags)]
    (println (time (->> bags
                        (transitive-containers "shiny gold")
                        count)))
    (println (time (->> bags
                        contents-by-container
                        (bags-inside "shiny gold"))))))
