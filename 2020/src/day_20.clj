(ns day-20
  (:require
   [clojure.set :as set]
   [clojure.string :as string]
   [instaparse.core :as insta]
   [multiset.core :as ms]
   [common]))

(def tile-list-parser
  (insta/parser
   "<tile-defs> = (tile-def <blank-line>)+
    blank-line  = '\n'
    tile-def    = id lines
    lines       = tile-line +
    id          = <'Tile '> #'[0-9]+' <':\n'>
    <tile-line> = #'[.#]+' <'\n'>"))

(defn parse-tiles [s]
  (->> s
       tile-list-parser
       (insta/transform
        {:lines #(vector :lines (vec %&))
         :id       #(vector :id (read-string %))
         :tile-def #(into {} %&)})))

(defn borders [lines]
  {:top (first lines)
   :bottom (last lines)
   :left (apply str (map first lines))
   :right (apply str (map last lines))})

(def flip
  "Flip top to bottom"
  reverse)

(defn rot
  "Rotate clockwise"
  [lines]
  (let [lines (reverse lines)]
    (mapv (fn [col] (apply str (map #(get % col) lines)))
          (range (count (first lines))))))

(def transformed
  (juxt identity       rot       (comp rot rot     ) (comp rot rot rot)
        flip     (comp rot flip) (comp rot rot flip) (comp rot rot rot flip)))

(defn add-variations [{:keys [id lines] :as tile}]
  (assoc tile
         :variations
         (mapv
          (fn [trf lines] {:lines lines :borders (borders lines) :trf trf :tile id})
          (range)
          (transformed lines))))

(def tile-defs
  (->> (common/input)
       slurp
       parse-tiles))

(def tiles
  (->> (into {}
             (map (fn [{:keys [id] :as tile}]
                    [id (add-variations tile)]))
             tile-defs)))

(defn group-by-borders [variations]
  (into {}
        (map (fn [b]
               [b (into {}
                        (map (fn [[k v]] [k (map #(select-keys % [:tile :trf]) v)]))
                        (group-by #(-> % :borders b) variations))]))
        [:top :right :bottom :left]))

(def available-borders
  (->> tiles
       vals
       (mapcat :variations)
       group-by-borders))

(def corner-tiles
  ;; To find corner tiles, we find tiles that have two borders that don't appear in any other tile
  (->> available-borders
       ;; Because of all the symetries, every possible tile border is reflected in :top, :right, :bottom, and :left so we just pick one
       :top

       ;; Tile borders that appear on only one tile have to be on the puzzle's border.
       ;; The converse is not generally true, but on this advent of code puzzle it is.
       (keep (fn [[_ [{:keys [tile]} & tiles]]] (when (empty? tiles) tile)))

       ;; Count how many times each tile appears
       (into (ms/multiset))
       (ms/multiplicities)

       ;; Tiles that are on the puzzle's edge (not in a corner) will appear twice: the border edge and its mirror version
       (keep (fn [[v m]] (when (= 4 m) v)))))

(def part-1
  (apply * corner-tiles))

(defn top-left-orientation
  "
  A suitable transformation id for putting the tile in the top left corner.
  Must be called with the id of a corner tile only.
  "
  [tile-id]
  (->> tile-id
       tiles
       :variations
       (keep (fn [{{:keys [bottom right]} :borders :keys [trf]}]
               ;; We know that we will find our own tile in the map.
               ;; Therefore, an inner border border will match at least two twiles: itself, and another tile's border.
               (when (and (-> available-borders :top  (get bottom) count (> 1))
                          (-> available-borders :left (get right)  count (> 1)))
                 trf)))
       first))

(defn top-left-tile []
  (let [tile-id           (first corner-tiles)
        transformation-id (top-left-orientation tile-id)]
    {:tile tile-id
     :trf  transformation-id}))

(defn matching-neighbour [from to {:keys [tile trf]}]
  (let [border (-> tiles (get tile) :variations (get trf) :borders from)
        tiles (-> available-borders to (get border))]
    (->> tiles (filter #(not= (:tile %) tile)) first)))

(defn right-neighbour [tile]
  (matching-neighbour :right :left tile))

(defn top-row []
  (take 12 (iterate right-neighbour (top-left-tile))))

(defn bottom-neighbour [tile]
  (matching-neighbour :bottom :top tile))

(defn row-below [row]
  (map bottom-neighbour row))

(defn complete-puzzle []
  (take 12 (iterate row-below (top-row))))

(defn crop [lines]
  (->> lines
       rest
       (take (-> lines count (- 2)))
       (map #(subs % 1 (-> % count dec)))))

(defn tile-for-puzzle [{:keys [tile trf]}]
  (-> tiles
      (get tile)
      :variations
      (get trf)
      :lines
      crop))

(defn concat-tiles [tiles]
  (apply map #(apply str %&) tiles))

(defn complete-puzzle-bitmap []
  (into []
        (comp
         (map #(map tile-for-puzzle %))
         (mapcat concat-tiles))
        (complete-puzzle)))

(defn match-positions [re s]
  (let [matcher (re-matcher re s)]
    (loop [start   0
           matches #{}]
      (if-not (.find matcher start)
        matches
        (let [start (.start matcher)]
          (recur (inc start)
                 (conj matches start)))))))

(def sea-monster
  ["                  # "
   "#    ##    ##    ###"
   " #  #  #  #  #  #   "])

(def sea-monster-res
  (map #(re-pattern (string/replace % #" " ".")) sea-monster))

(defn find-sea-monsters [lines]
  (let [matches (map match-positions sea-monster-res lines)]
    (apply set/intersection matches)))

(defn all-sea-monsters-in [lines]
  (mapcat (fn [row lines]
            (map (fn [col] [row col])
                 (find-sea-monsters lines)))
          (range)
          (partition 3 1 lines)))

(defn righted-puzzle-and-sea-monster-positions []
  (first (keep #(when-some [positions (seq (all-sea-monsters-in %))]
                  [% positions])
               (transformed (complete-puzzle-bitmap)))))

(def sea-monster-offsets
  (mapcat
   (fn [row line]
     (keep-indexed
      (fn [col char] (when (= \# char) [row col]))
      line))
   (range)
   sea-monster))

(defn highlight-sea-monster [lines [row col]]
  (reduce (fn [lines [row-offset col-offset]]
            (update lines (+ row row-offset)
                    (fn [line]
                      (assoc (vec line) (+ col col-offset) \O))))
          lines
          sea-monster-offsets))

(defn highlight-sea-monsters [lines sea-monster-positions]
  (map #(apply str %)
       (reduce highlight-sea-monster lines sea-monster-positions)))

(defn roughness [lines]
  (apply + (map #(count (filter #{\#} %)) lines)))

(defn part-2 []
  (roughness
   (apply highlight-sea-monsters (righted-puzzle-and-sea-monster-positions))))

(defn -main []
  (println part-1)
  (println (time (part-2))))
