(ns day-19
  (:require
   [common]
   [clojure.string :as string]
   [instaparse.core :as insta]))

(defn part-1 []
  (let [[rules input] (-> (common/input) slurp (string/split #"\n\n"))
        parser (insta/parser rules :start :0)
        messages (string/split input #"\n")]
    (count (filter #(not (insta/failure? (parser %))) messages))))

(defn part-2 []
  (let [[rules input] (-> (common/input) slurp (string/split #"\n\n"))
        rules (-> rules
                  (string/replace #"(?m)^8:.*$" "8: 42 | 42 8")
                  (string/replace #"(?m)^11:.*$" "11: 42 31 | 42 11 31"))
        parser (insta/parser rules :start :0)
        messages (string/split input #"\n")]
    (count (filter #(not (insta/failure? (parser %))) messages))))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
