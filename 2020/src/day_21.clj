(ns day-21
  (:require
   [common]
   [clojure.set :as set]
   [clojure.string :as string]
   [instaparse.core :as insta]))

(def input-parser
  (insta/parser
   "<food-list> = (food <'\n'>)+
    food = ingredients <' '> allergens
    ingredients = word ( <' '> word )*
    allergens = <'(contains '> word ( <', '> word )* <')'>
    <word> = #'[a-z]+' "))

(defn parse-input [s]
  (->> s
       input-parser
       (insta/transform
        {:ingredients (fn [& is] [:ingredients (set is)])
         :allergens   (fn [& as] [:allergens   (set as)])
         :food        #(into {} %&)})))

(def input
  (-> (common/input) slurp parse-input))

(def possible-ingredients-per-allergen
  (->> input
       (map (fn [{:keys [allergens ingredients]}]
              (into {} (map vector allergens (repeat ingredients)))))
       (apply merge-with set/intersection)))

(def all-ingredients
  (mapcat :ingredients input))

(def suspect-ingredients
  (apply set/union (vals possible-ingredients-per-allergen)))

(def safe-ingredients
  (filter (complement suspect-ingredients) all-ingredients))

(def part-1
  (count safe-ingredients))

(defn unambiguous-allergens [ingredients-per-allergen]
  (->> ingredients-per-allergen
       (keep (fn [[k [v & vs]]] (when-not (seq vs) [k v])))))

(defn pinpoint-allergens [{:keys [attributed unattributed]}]
  (let [new-allergens (unambiguous-allergens unattributed)
        allergens (map first new-allergens)
        ingredients (into #{} (map second) new-allergens)]
    {:attributed (into attributed new-allergens)
     :unattributed (->> (apply dissoc unattributed allergens)
                        (into {} (map (fn [[k v]] [k (set/difference v ingredients)]))))}))

(defn fixed-point
  [f x0]
  (->> x0
       (iterate f)
       (partition 2 1)
       (drop-while #(apply not= %))
       ffirst))

(def allergen-to-ingredient
  (->
   (fixed-point pinpoint-allergens {:attributed {} :unattributed possible-ingredients-per-allergen})
   :attributed))

(def allergen-list
  (sort (keys allergen-to-ingredient)))

(def canonical-ingredient-list
  (->> allergen-list
       (map allergen-to-ingredient)
       (string/join ",")))

(defn -main []
  (println part-1)
  (println canonical-ingredient-list))
