(ns day-06
  (:require
   [common]
   [clojure.set :as set]
   [clojure.string :as string]))

(defn ->answer-set [answers]
  (-> answers set (disj \newline)))

(defn total-count [answer-sets]
  (->> answer-sets (map count) (apply +)))

(defn common-answers [answers]
  (-> answers
      (string/split #"\n")
      (->> (map ->answer-set)
           (apply set/intersection))))

(defn -main []
  (let [by-group (-> (common/input)
                     slurp
                     (string/split #"\n\n"))]
    (println (time (->> by-group (map ->answer-set) total-count)))
    (println (time (->> by-group (map common-answers) total-count)))))
