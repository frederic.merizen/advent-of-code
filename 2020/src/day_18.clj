(ns day-18
  (:require
   [instaparse.core :as insta]
   [common]))

;; Helpers for debugging grammars
(defn ambiguous-expressions [parser]
  (filter #(< 1 (count (insta/parses parser %)))
          expressions))

(defn invalid-expressions [parser]
  (filter #(empty? (insta/parses parser %))
          expressions))

;; The grammars have left recursions such as (expression -> sum -> expression + argument),
;; but instaparse is fine with that

(def expression-parser
  (insta/parser
   "<expression> = argument | sum | product
    <argument> = constant | (<'('> expression <')'>)
    constant = #'[0-9]'
    sum = expression <' + '> argument
    product = expression <' * '> argument"))

(defn evaluate [expression]
  (->> expression
       (insta/transform
        {:constant read-string
         :sum +
         :product *})
       first))

(def expressions
  (common/lines (common/input)))

(defn sum-of-answers [parser]
  (apply + (map (comp evaluate parser) expressions)))

(defn part-1 []
  (sum-of-answers expression-parser))


;; The following ambiguous grammar also does the job, but, while being able to use a
;; left-recursive grammar felt cool, using an ambiguous grammar felt dirty, so, no.
;;
;; <expression> = product | argument
;; <argument> = constant | sum | (<'('> expression <')'>)
;; constant = #'[0-9]'
;; sum = argument <' + '> argument
;; product = expression <' * '> argument"

(def advanced-parser
  (insta/parser
   "<expression> = term | product
    <term> = argument | sum
    <argument> = constant | (<'('> expression <')'>)
    constant = #'[0-9]'
    sum = term <' + '> argument
    product = expression <' * '> term"))

(defn part-2 []
  (sum-of-answers advanced-parser))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
