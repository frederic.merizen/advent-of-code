(ns quil-intro
  (:require [quil.core :as q]))

(defn draw []
  (q/background 255)
  (q/with-translation [(/ (q/width) 2) (/ (q/height) 2)]
    (draw-plot f 0 100 0.01)))

(defn draw []
  (q/with-translation [(/ (q/width) 2) (/ (q/height) 2)]
                                        ; note that we don't use draw-plot here as we need
                                        ; to draw only small part of a plot on each iteration
    (let [t (/ (q/frame-count) 10)]
      (q/line (f t)
              (f (+ t 0.1))))))
(defn setup []
                                        ; draw will be called 60 times per second
  (q/frame-rate 60)
                                        ; set background to white colour only in the setup
                                        ; otherwise each invocation of 'draw' would clear sketch completely
  (q/background 255))

(q/defsketch trigonometry
  :size [300 300]
  :setup setup
  :draw draw)
