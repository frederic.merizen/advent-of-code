(ns day-15)

(defn nth-move [starting-numbers ^long index]
  (let [end            (dec index)]
    (loop [i           (count starting-numbers)
           prev        0
           last-played (reduce (fn [m [v k]] (assoc! m k v))
                               (transient {})
                               (map-indexed vector starting-numbers))]
      (if (= i end)
        prev
        (recur (inc i)
               (if-let [^long j (last-played prev)]
                 (- i j)
                 0)
               (assoc! last-played prev i))))))

(def input [0,12,6,13,20,1,17])

(defn -main []
  (println (time (nth-move input 2020)))
  (println (time (nth-move input 30000000))))

#?(:cljs (-main))
