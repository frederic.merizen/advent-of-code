(ns day-10
  (:require
   [common]))

(def input (->> (common/input) (common/lines) (map read-string)))

(def sorted (vec (sort input)))

(defn hops []
  (concat
   [(first sorted)] ; From outlet to first adapter
   (map (fn [[a b]] (- b a)) (partition 2 1 sorted))
   [3]))           ; From last adapter to device

(defn hop-counts []
  (into {}
        (map (fn [[hop hops]] [hop (count hops)]))
        (group-by identity (hops))))

(defn part-1 []
  (let [counts (hop-counts)]
    (* (counts 1) (counts 3))))

(def device-adapter (+ 3 (last sorted)))

(def adapters
  (conj sorted device-adapter))

(defn solution-count []
  (reduce (fn [counts adapter]
            (assoc counts adapter (apply + (keep counts (range (- adapter 3) adapter)))))
          {0 1}
          adapters))

(defn part-2 []
  ((solution-count) device-adapter))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
