(ns day-05
  (:require
   [common]
   [clojure.set :as set]))

(defn decode [seat-code]
  (Integer/parseInt (apply str (map {\F \0 \B \1 \L 0 \R \1} seat-code)) 2))

(def claimed-seats
  (->> (common/input)
       common/lines
       (map decode)
       set))

(defn first-seat [] (apply min claimed-seats))

(defn last-seat [] (apply max claimed-seats))

(defn forwards [seat] (- seat 8))
(defn backwards [seat] (+ seat 8))
(defn leftmost [seat] (bit-and seat -8))
(defn rightmost [seat] (bit-or seat 7))

(defn candidate-seats [] (set (range (-> (first-seat) backwards leftmost)
                                     (-> (last-seat) forwards rightmost))))

(defn my-seat [] (first (set/difference (candidate-seats) claimed-seats)))

(defn -main []
  (println (time (last-seat)))
  (println (time (my-seat))))
