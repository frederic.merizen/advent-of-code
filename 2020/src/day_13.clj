(ns day-13
  (:require
   [clojure.string :as string]
   [common]))

(def lines (-> (common/input) common/lines))

(def arrival (-> lines first read-string))
(def id-line (-> lines fnext (string/split #",") (->> (map #(when-not (= "x" %) (read-string %))))))
(def ids (filter identity id-line))

(defn wait-time [arrival id]
  (mod (- arrival) id))

(defn part-1 []
  (let [id (apply min-key #(wait-time arrival %) ids)]
    (* id (wait-time arrival id))))

(defn gcd
  "
  Extended Euclid algorithm

  Returns [r u v] such that
  - r = gcd(a, b)
  - a*u + b*v = r
  "
  [a b]
  (loop [r  a, u  1, v  0
         r' b, u' 0, v' 1]
    (if (zero? r')
      [r u v]
      (let [q (quot r r')]
        (recur r',             u',             v'
               (- r (* q r')), (- u (* q u')), (- v (* q v')))))))

(defn chinese
  "
  Solves the chinese remainder problem: returns x such that

  x ≡ a1 (mod n1)
  ...
  x ≡ as (mod ns)

  n1, ..., ns must be positive integers and pairwise relative prime
  "
  [as ns]
  (let [N (apply * ns)]
    (-> (apply + (map (fn [a n]
                        (let [mul (/ N n)
                              [_ inv _] (gcd mul n)]
                          (* a mul inv)))
                      as ns))
        (mod N))))

(defn part-2 []
  (chinese
   (keep-indexed #(when %2 (- %1)) id-line)
   ids))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
