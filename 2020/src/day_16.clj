(ns day-16
  (:require
   [clojure.string :as string]
   [instaparse.core :as insta]
   [common]))

(def prog-parser
  (insta/parser
   "input = rules <blank-line> your-ticket <blank-line> nearby-tickets
    blank-line = '\n'

    rules = rule *
    rule = name <': '> range <' or '> range <'\n'>
    name = #'[a-z ]+'
    range = number <'-'> number
    number = #'[0-9]+'

    your-ticket = <'your ticket:\n'> ticket

    nearby-tickets = <'nearby tickets:\n'> ticket *

    ticket = number ( <','> number) * <'\n'> "))

(def input
  (->> (common/input)
       slurp
       prog-parser
       (insta/transform
        {:number read-string
         :range #(set (range %1 (inc %2)))
         :rule (fn [[_ name] r1 r2] {:name name :valid (into r1 r2)})
         :rules (fn [& rules] [:rules (vec rules)])
         :nearby-tickets (fn [& nearby-tickets] [:nearby-tickets (vec nearby-tickets)])
         :ticket vector
         :input #(into {} %&)})))

(defn invalid-field? [v]
  (not (some #(% v) (map :valid (:rules input)))))

(defn part-1 []
  (->> input
       :nearby-tickets
       (apply concat)
       (filter invalid-field?)
       (apply +)))

(defn valid-ticket? [t]
  (not (some invalid-field? t)))

(defn keep-fields-compatible-with-value [value fields]
  (filter #((:valid %) value) fields))

(defn keep-fields-compatible-with-ticket [field-lists values]
  (map keep-fields-compatible-with-value values field-lists))

(defn valid-tickets []
  (->> input
       :nearby-tickets
       (filter valid-ticket?)))

(defn possible-fields-per-position []
  (reduce keep-fields-compatible-with-ticket
          (repeat (-> input :rules))
          (valid-tickets)))

(defn possible-positions-per-field-name []
  (->> (possible-fields-per-position)
       (map-indexed (fn [pos fields] (map (fn [{:keys [name]}] {:name name, :pos pos}) fields)))
       (apply concat)
       (group-by :name)
       (into {} (map (fn [[k v]] [k (into #{} (map :pos v))])))))

(defn unambiguous-field-pos [positions-per-field-name]
  (->> positions-per-field-name
       (keep (fn [[k [v & vs]]] (when-not (seq vs) [k v])))
       first))

(defn disambiguate-one-field [{:keys [attributed unattributed]}]
  (let [[name pos] (unambiguous-field-pos unattributed)]
    {:attributed (assoc attributed name pos)
     :unattributed (-> unattributed
                       (dissoc name)
                       (->> (into {} (map (fn [[k v]] [k (disj v pos)])))))}))

(defn fixed-point
  [f x0]
  (->> x0
       (iterate f)
       (partition 2 1)
       (drop-while #(apply not= %))
       ffirst))

(defn position-per-field-name []
  (->
   (fixed-point disambiguate-one-field {:attributed {} :unattributed (possible-positions-per-field-name)})
   :attributed
   (dissoc nil)))

(defn positions-for-columns-with-prefix [prefix]
  (keep (fn [[name pos]] (when (string/starts-with? name prefix) pos))
        (position-per-field-name)))

(defn departure-vals []
  (let [ticket (:your-ticket input)]
    (map #(ticket %)(positions-for-columns-with-prefix "departure "))))


(defn part-2 []
  (apply * (departure-vals)))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
