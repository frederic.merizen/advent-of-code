(ns day-23
  (:require
   [clojure.data.int-map :as i]))

(defn move [{:keys [curr succ max-cup] :as state}]
  (let [picked-up (take 3 (rest (iterate succ curr)))
        forbidden (set picked-up)
        dest (->> curr (iterate #(-> % (- 2) (mod max-cup) inc)) rest (drop-while forbidden) first)
        next-curr (succ (last picked-up))]
    (assoc state
           :curr next-curr
           :succ (assoc succ
                        curr next-curr
                        dest (first picked-up)
                        (last picked-up) (succ dest)))))

(defn score [{:keys [succ]}]
  (apply str (->> (iterate succ 1)
                  (drop 1)
                  (take 8))))

(def part-1-cups (map (comp read-string str) "538914762"))

(defn succ-map [cups]
  {:max-cup (count cups)
   :curr (first cups)
   :succ (into (i/int-map)
               (map vector
                    cups
                    (rest (cycle cups))))})

(defn play-game [rounds cups]
  (->> cups
       succ-map
       (iterate move)
       (drop rounds)
       first))

(defn part-1 []
  (->> part-1-cups
       (play-game 100)
       score))

(defn part-2 []
  (let [{:keys [succ]} (->> (concat part-1-cups (range 10 1000001))
                            (play-game 10000000))]
    (* (succ 1) (succ (succ 1)))))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
