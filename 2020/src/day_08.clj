(ns day-08
  (:require
   [common]
   [instaparse.core :as insta]))

(def code-parser
  (insta/parser
   "code        = (instruction <'\n'>)*
    instruction = op <' '> arg
    op          = #'[a-z]+'
    arg         = #'[+-][0-9]+'"))

(defn parse-code [s]
  (->> s
       code-parser
       (insta/transform
        {:op          (fn [o] [:op (keyword o)])
         :arg         (fn [a] [:arg (Integer/parseInt a)])
         :instruction #(into {} %&)
         :code        vector})))

(def operations
  {:acc (fn [regs op] (update regs :acc + op))
   :jmp (fn [regs op] (update regs :pc + (dec op)))
   :nop (fn [regs _] regs)})

(defn run-inst [regs {:keys [op arg]}]
  (-> regs
      ((operations op) arg)
      (update :pc inc)))

(defn track-loop [regs inst]
  (let [{:keys [visited pc] :as regs} (run-inst regs inst)]
    (assoc regs
        :outcome (when (get visited pc) :loops)
        :visited (conj (or visited #{}) pc))))

(defn step [{:keys [pc] :as regs} program]
  (let [end (count program)]
    (cond
      (> pc end) (assoc regs :outcome :out-of-bounds)
      (= pc end) (assoc regs :outcome :terminates)
      :else      (track-loop regs (program pc)))))

(def init {:acc 0, :pc 0})

(defn run [program]
  (->> init
       (iterate #(step % program))
       (filter :outcome)
       first))

(defn mutate [inst]
  (update inst :op {:acc :acc, :nop :jmp, :jmp :nop}))

(defn mutated-programs [program]
  (map #(update program % mutate) (range (count program))))

(defn run-fixed [program]
  (->> (mutated-programs program)
       (map run)
       (filter #(= :terminates (:outcome %)))
       first))

(defn -main []
  (let [program (-> (common/input) slurp parse-code)]
    (println (time (:acc (run program))))
    (println (time (:acc (run-fixed program))))))
